Feature: Tuya Powerstrip

  Scenario: Powerstrip includes all needed functions
  Given Preconfigured account with Tuya Powerstrip
    When Entering the Powerstrip tab
    Then All Powerstrip functions are visible

  Scenario: Powerstrip settings tab and name change work correctly
  Given Preconfigured account with Tuya Powerstrip
    When Entering the Powerstrip settings tab
    And Changes the Powerstrip name
    Then New Powerstrip name is visible

  Scenario Outline: Add and remove Powerstrip features from the dashboard
    Given Preconfigured account with Tuya Powerstrip
    When Add Powerstrip <function> to the dashboard
    And Remove <function> from the dashboard
    Then <function> is not visible in the dashboard

    Examples:
    |             function                    |
    |             Zasilanie nr 1              |
    |             Zasilanie nr 2              |
    |             Zasilanie nr 3              |
    |             Wejścia USB                 |
    |             Wszystkie przełączniki      |

  Scenario: Add Powerstrip to the room
    Given Preconfigured account with Tuya Powerstrip
    When User adds Powerstrip to the room via room settings
    Then Powerstrip is visible in the room settings

  Scenario: Remove Powerstrip from the room
    Given Preconfigured account with Tuya Powerstrip
    When User removes Powerstrip from the room
    Then Powerstrip is not visible in the room settings
