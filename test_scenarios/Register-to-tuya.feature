Feature: Register to Tuya

  Scenario: Tuya register with correct data
    Given Tuya register screen is displayed
    When User inserts correct e-mail and passwords
    Then Tuya after-register screen is displayed

  Scenario Outline: Tuya register with incorrect email
    Given Tuya register screen is displayed
    When User inserts e-mail <email> and passwords <password> <confirm_password>
    Then Info about incorrect e-mail is displayed

    Examples: Users
    |     email     |    password     |    confirm_password     |
    |     aaa       |    Haslomaslo1  |    Haslomaslo1          |
    |     @slabs.pl |    Aaaaaaa8     |    Aaaaaaa8             |
    |     abc.pl.pl |    Okonokon3    |    Okonokon3            |

  Scenario Outline: Tuya register with correct email and incorrect passwords
    Given Tuya register screen is displayed
    When User inserts e-mail <email> and passwords <password> <confirm_password>
    Then Info about incorrect password is displayed

    Examples:
    |    email             |    password     |    confirm_password     |
    |    parar@wp.pl       |    aaa          |    aaa                  |
    |    szeryf08@slabs.pl |    aaaaaaa8     |    aaaaaaa8             |
    |    bolek@abc.com     |    3333333AA    |    3333333AA            |

  Scenario Outline: Tuya register with correct email and different passwords
    Given Tuya register screen is displayed
    When User inserts e-mail <email> and passwords <password> <confirm_password>
    Then Info about different passwords is displayed

    Examples:
    |    email             |    password     |    confirm_password     |
    |    parar@wp.pl       |    Haslomaslo1  |    Haslomaslo2          |
    |    szeryf08@slabs.pl |    Aaaaaaa1     |    Aaaaaaa2             |
    |    bolek@abc.com     |    3333333Aa    |    3333333Ab            |


  Scenario Outline: Tuya register with empty email or empty passwords
    Given Tuya register screen is displayed
    When User inserts e-mail <email> and passwords <password> <confirm_password>
    Then Info about empty field is displayed

    Examples:
    |    email             |    password     |    confirm_password     |
    |    empty             |    Haslomaslo1  |    Haslomaslo1          |
    |    szeryf08@slabs.pl |    empty        |    Aaaaaaa2             |
    |    bolek@abc.com     |    3333333Aa    |    empty                |
    |    empty             |    empty        |    Haslomaslo1          |
    |    szeryf08@slabs.pl |    empty        |    empty                |
    |    empty             |    Haslomaslo1  |    empty                |
    |    empty             |    empty        |    empty                |


  Scenario: Tuya register without internet connection
    Given Tuya register screen is displayed
    When Wifi is turned off
    And User inserts correct e-mail and passwords
    Then Info about no internet connection is displayed
