from lib.helpers import get_string_hash
from lib.logger import get_logger
from lib.config import *
from lib.rest_user import RestUser
from screen_models.device_settings_scrn import DevicesSettingsScrn
from screen_models.login_scrn import LoginScrn
from screen_models.register_scrn import RegisterScrn
from screen_models.spaces_scrn import SpacesScrn
from screen_models.dashboard_scrn import DashboardScrn
from screen_models.new_space_scrn import NewSpaceScrn
from screen_models.rooms_scrn import RoomsScrn
from lib.device_driver import DeviceDriver
from screen_models.devices_scrn import DevicesScrn
import uiautomator2 as u2

logger = get_logger(__name__)


def before_all(context):
    logger.debug('Executing before all')
    logger.debug('Connecting to the device')
    dev = DeviceDriver(u2.connect())
    context.driver = dev
    context.driver.device.set_fastinput_ime(True)
    logger.debug('Device info: {}'.format(context.driver.device.info))
    context.driver.device.implicitly_wait(DEFAULT_TIMEOUT_S)

    context.rest_user = RestUser()
    context.rest_user.generate_admin_token()
    reponse_user = context.rest_user.register_account()
    context.rest_user.user_id = reponse_user.json()['response']['id']
    context.rest_user.activate_account(reponse_user.json()['response']['id'])
    reponse_space = context.rest_user.create_space(context.rest_user.user_id)
    context.space_name = reponse_space.json()['response']['name']
    context.space_id = reponse_space.json()['response']['id']
    reponse_room = context.rest_user.create_zone(context.rest_user.user_id, context.space_id)
    context.room_name = reponse_room.json()['response']['name']

    context.login_scrn = LoginScrn("Login_Screen", context.driver)
    context.register_scrn = RegisterScrn("Register_Screen", context.driver)
    context.spaces_scrn = SpacesScrn("Spaces_Screen", context.driver)
    context.new_space_scrn = NewSpaceScrn("New_Space_Screen", context.driver)
    context.dashboards_scrn = DashboardScrn("Dashboards_Screen", context.driver)
    context.rooms_scrn = RoomsScrn("Rooms_Screen", context.driver)
    context.devices_scrn = DevicesScrn('Devices Screen', context.driver)
    context.sensor_scrn = DevicesSettingsScrn('Sensor Screen', context.driver, device_name=TUYA_SENSOR_NAME,
                                               device_room=TUYA_SENSOR_ROOM, device_space=SPACE_NAME)
    context.plug_scrn = DevicesSettingsScrn('Plug Screen', context.driver, device_name=PLUG_NAME,
                                               device_room=PLUG_ROOM, device_space=SPACE_NAME)
    context.pm_detector_scrn = DevicesSettingsScrn('PM Detector Screen', context.driver, device_name=PM_DETECTOR_NAME,
                                               device_room=PM_DETECTOR_ROOM, device_space=SPACE_NAME)
    context.gateway_scrn = DevicesSettingsScrn("Gateway Screen", context.driver, device_name=TUYA_GATEWAY_NAME,
                                               device_room=TUYA_SENSOR_ROOM, device_space=SPACE_NAME)
    context.contactron_scrn = DevicesSettingsScrn("Contactron Screen", context.driver, device_name=TUYA_CONTACTRON_NAME,
                                                  device_room=TUYA_CONTACTRON_ROOM, device_space=SPACE_NAME)
    context.trv_scrn = DevicesSettingsScrn("TRV Screen", context.driver, device_name=TUYA_TRV_NAME,
                                                  device_room=TUYA_TRV_ROOM, device_space=SPACE_NAME)
    context.powerstrip_scrn = DevicesSettingsScrn("Powerstrip Screen", context.driver, device_name=TUYA_POWERSTRIP_NAME,
                                           device_room=TUYA_POWERSTRIP_ROOM, device_space=SPACE_NAME)


def before_feature(context, feature):
    logger.debug('Executing before feature')
    session = context.driver.device.session(APP_VARIANT, attach=True)
    context.driver.set_session(session)


def before_scenario(context, scenario):
    logger.debug('Executing before scenario')
    context.driver.turn_on_wifi()
    logger.debug('Clearing the app')
    context.driver.device.app_clear(APP_VARIANT)
    context.driver.device.app_start(APP_VARIANT)


def after_scenario(context, scenario):
    logger.debug('Executing after scenario')
    logger.debug('Stopping the app')
    context.driver.device.app_stop(APP_VARIANT)


def after_feature(context, feature):
    logger.debug('Executing after feature')


def after_all(context):
    logger.debug('Executing after all')
    if context.rest_user:
        context.rest_user.delete_user_account(context.rest_user.user_id)
        context.rest_user.user_token = context.rest_user.\
            login_and_get_token({'email': USER_EMAIL[:-1], 'password_hash': get_string_hash(USER_PASSWORD[:-1])})
        dashboard_id = context.rest_user.get_dashboard_id_by_name(USER_ID, SPACE_ID, 'TestDashboard')
        context.rest_user.delete_dashboard(USER_ID, SPACE_ID, dashboard_id)
        context.rest_user.create_dashboard(USER_ID, SPACE_ID, {"name": "TestDashboard"})
