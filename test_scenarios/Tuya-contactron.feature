Feature: Tuya Contactron

  Scenario: Contactron includes all needed functions
  Given Preconfigured account with Tuya Contactron
    When Entering the contactron tab
    Then All contactron functions are visible

  Scenario: Contactron settings tab and name change work correctly
  Given Preconfigured account with Tuya Contactron
    When Entering the contactron settings tab
    And Changes the contactron name
    Then New contactron name is visible

  Scenario Outline: Add and remove contactron features from the dashboard
    Given Preconfigured account with Tuya Contactron
    When Add contactron <function> to the dashboard
    And Remove <function> from the dashboard
    Then <function> is not visible in the dashboard

    Examples:
    |             function                    |
    |             Bateria                     |
    |             Czujnik otwarcia            |

  Scenario: Add contactron to the room
    Given Preconfigured account with Tuya Contactron
    When User adds contactron to the room via room settings
    Then Contactron is visible in the room settings

  Scenario: Remove contactron from the room
    Given Preconfigured account with Tuya Contactron
    When User removes contactron from the room
    Then Contactron is not visible in the room settings
