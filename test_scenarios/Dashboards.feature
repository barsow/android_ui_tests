Feature: Dashboards

  Scenario: Creating dashboards
    Given Configured user account opened on dashboard tab
    When User clicks new dashboard button
    And User inserts correct dashboard name Dashboard1
    Then Dashboard1 is in the dashboards settings screen

  Scenario: Creating next dashboards
    Given Configured user account with a dashboard opened
    When User clicks new dashboard button
    And User inserts correct dashboard name Next Dashboard
    Then Next Dashboard is in the dashboards settings screen

  Scenario: Deleting dashboard
    Given Configured user account with a dashboard opened
    When User deletes a dashboard
    Then Screen with no dashboard is displayed

  Scenario: Editing dashboard with correct data
    Given Configured user account with a dashboard opened
    When User changes dashboard name to 3dited D@shb04rd!
    Then 3dited D@shb04rd! is in the dashboards settings screen
    And Screen with no dashboard is displayed

  Scenario Outline: Editing dashboard with incorrect names
    Given Configured user account with a dashboard opened
    When User edits dashboard name to <incorrect>
    Then Approriate action for <incorrect> name is performed
        Examples: Users
        |   incorrect   |
        |   empty       |
        |only_whitespace|
        | too_long      |
