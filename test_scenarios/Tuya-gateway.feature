Feature: Tuya Gateway

  Scenario: Gateway includes all needed functions
  Given Preconfigured account with Tuya Gateway
    When Entering the Gateway tab
    And Entering the Gateway settings
    Then All gateway settings are visible

  Scenario: Add gateway to the room
    Given Preconfigured account with Tuya Gateway
    When User adds gateway to the room via room settings
    Then Gateway is visible in the room settings

  Scenario: Remove gateway from the room
    Given Preconfigured account with Tuya Gateway
    When User removes gateway from the room
    Then Gateway is not visible in the room settings
