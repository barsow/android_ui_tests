Feature: Login to tuya

  Scenario: Tuya login with correct data
    Given Tuya Login screen is displayed
    When User inserts correct tuya e-mail and password
    Then Empty dashboard screen is displayed

  Scenario: Tuya login and logout
    Given Tuya Login screen is displayed
    When User inserts correct tuya e-mail and password
    And User logouts from tuya
    Then User is logged out

  Scenario Outline: Tuya login with incorrect e-mail and correct password
    Given Tuya Login screen is displayed
    When User inserts e-mail <email> and password <password>
    Then Info about incorrect e-mail is displayed

    Examples: Users
        |   email   |   password    |
        |   aaa     |   Okonokon2   |
        |   a@@@a   |   Haslomaslo3 |
        |   @sl.pl  |   Aaaaaa567   |

  Scenario Outline: Tuya login with correct e-mail and incorrect password
    Given Tuya Login screen is displayed
    When User inserts e-mail <email> and password <password>
    Then Info about incorrect password is displayed

    Examples: Users
        |   email                      |   password    |
        |   automatic_tester@wp.pl     |   aaa         |
        |   wololo@gmail.com           |   33333333333 |
        |   szeryf@slabs.pl            |   AAAAA555    |

  Scenario Outline: Tuya login with incorrect e-mail and incorrect password
    Given Tuya Login screen is displayed
    When User inserts e-mail <email> and password <password>
    Then Info about incorrect e-mail is displayed
    And Info about incorrect password is displayed

    Examples: Users
        |   email                      |   password    |
        |   aaa                        |   !!!         |
        |   wololo@.com                |   33333333333 |
        |   @slabs.pl                  |   AAAAA555    |

  Scenario Outline: Tuya login with empty e-mail or empty password
    Given Tuya Login screen is displayed
    When User inserts e-mail <email> and password <password>
    Then Info about empty field is displayed

    Examples: Users
        |   email                      |   password    |
        |   empty                      |   Haslomaslo3 |
        |   wololo@clabs.com           |   empty       |
        |   empty                      |   empty       |

  Scenario: Tuya login without internet connection
    Given Tuya Login screen is displayed
    When Wifi is turned off
    And User inserts correct tuya e-mail and password
    Then Info about no internet connection is displayed
