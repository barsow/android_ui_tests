Feature: Login to appartme

  Scenario: Login with correct data
    Given Login screen is displayed
    When User inserts correct appartme e-mail and password
    Then Screen with available spaces is displayed

  Scenario Outline: Login with incorrect e-mail and correct password
    Given Login screen is displayed
    When User inserts e-mail <email> and password <password>
    Then Info about incorrect e-mail is displayed

    Examples: Users
        |   email   |   password    |
        |   aaa     |   Okonokon2   |
        |   a@@@a   |   Haslomaslo3 |
        |   @sl.pl  |   Aaaaaa567   |

  Scenario Outline: Login with correct e-mail and incorrect password
    Given Login screen is displayed
    When User inserts e-mail <email> and password <password>
    Then Info about incorrect password is displayed

    Examples: Users
        |   email                      |   password    |
        |   automatic_tester@wp.pl     |   aaa         |
        |   wololo@gmail.com           |   33333333333 |
        |   szeryf@slabs.pl            |   AAAAA555    |

  Scenario Outline: Login with incorrect e-mail and incorrect password
    Given Login screen is displayed
    When User inserts e-mail <email> and password <password>
    Then Info about incorrect e-mail is displayed
    And Info about incorrect password is displayed

    Examples: Users
        |   email                      |   password    |
        |   aaa                        |   !!!         |
        |   wololo@.com                |   33333333333 |
        |   @slabs.pl                  |   AAAAA555    |

  Scenario Outline: Login with empty e-mail or empty password
    Given Login screen is displayed
    When User inserts e-mail <email> and password <password>
    Then Info about empty field is displayed

    Examples: Users
        |   email                      |   password    |
        |   empty                      |   Haslomaslo3 |
        |   wololo@clabs.com           |   empty       |
        |   empty                      |   empty       |


  Scenario: Login without internet connection
      Given Login screen is displayed
      When Wifi is turned off
      And User inserts correct appartme e-mail and password
      Then Info about no internet connection is displayed

