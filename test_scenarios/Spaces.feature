Feature: Spaces

  Scenario: Creating first space
    Given Space screen after login is displayed
    When User clicks new space button on spaces screen
    And User inserts correct space name Space1
    And User chooses space Space1
    Then Space1 space is in the Accounts tab

  Scenario Outline: Creating space with incorrect space name
    Given Space screen after login is displayed
    When User clicks new space button on spaces screen
    And User inserts incorrect space name <space_name>
    Then Info about incorrect space name is displayed
    Examples: Users
      |  space_name     |
      |  empty          |
      |  only_whitespace|

  Scenario: Creating space with no internet connection
    Given Space screen after login is displayed
    When User clicks new space button on spaces screen
    And Wifi is turned off
    And User inserts correct space name Space1
    Then Info about no internet connection is displayed

  Scenario: Creating next space
    Given Accounts tab is displayed for space Space1
    When User clicks new space button on accounts screen
    And User inserts correct space name Space2
    Then Space2 space is in the Accounts tab

  Scenario: Editing space name with correct name
    Given Accounts tab is displayed for space Space2
    When User changes space name to Space3
    Then Space3 space is in the Accounts tab

  Scenario Outline: Editing space name with incorrect name
    Given Accounts tab is displayed for space Space3
    When User edits name to incorrect space name <space_name>
    Then Info about incorrect space name is displayed
        Examples: Users
      |  space_name     |
      |  empty          |
      |  only_whitespace|


  Scenario: Deleting existing spaces
    Given Accounts tab is displayed for space Space1
    When User deletes spaces Space1 Space3
    Then Screen with no Space1 or Space3 is displayed

