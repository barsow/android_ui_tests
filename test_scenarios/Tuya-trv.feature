Feature: Tuya TRV

  Scenario: TRV includes all needed functions
  Given Preconfigured account with Tuya TRV
    When Entering the TRV tab
    Then All TRV functions are visible

  Scenario: TRV settings tab and name change work correctly
  Given Preconfigured account with Tuya TRV
    When Entering the TRV settings tab
    And Changes the TRV name
    Then New TRV name is visible

  Scenario Outline: Add and remove TRV features from the dashboard
    Given Preconfigured account with Tuya TRV
    When Add trv <function> to the dashboard
    And Remove <function> from the dashboard
    Then <function> is not visible in the dashboard

    Examples:
    |             function                    |
    |             Temperatura                 |
    |             Bateria                     |
#    |             Ustawiona temperatura       |
#    |             Nadchodzące zdarzenie       |

  Scenario: Add TRV to the room
    Given Preconfigured account with Tuya TRV
    When User adds TRV to the room via room settings
    Then TRV is visible in the room settings

  Scenario: Remove TRV from the room
    Given Preconfigured account with Tuya TRV
    When User removes TRV from the room
    Then TRV is not visible in the room settings
