from behave import *
from lib.config import *


@given("Preconfigured tuya account with demanded device")
def step_impl(context):
    context.login_scrn.login_user(USER_EMAIL, USER_PASSWORD)
    context.driver.session(text=SPACE_NAME).click()
    context.spaces_scrn.account_tab().click()
    context.driver.implicitly_wait()
    context.spaces_scrn.tuya_login_arrow().click()
    context.login_scrn.login_user(TUYA_EMAIL, TUYA_PASSWORD)
    context.devices_scrn.devices_tab().click()
    context.driver.implicitly_wait()
    assert context.devices_scrn.search_element_in_list(context.driver.session(text=context.current_device_name)) is True


@when("Entering the device tab")
def step_impl(context):
    context.driver.session(text=context.current_device_name).click()
    context.driver.implicitly_wait()


@when(u'Add the {function} of the device to the dashboard')
def step_impl(context, function):
    context.driver.session(resourceId=APP_VARIANT + ":id/dashboardsFragment").click()
    context.driver.implicitly_wait()
    context.dashboards_scrn.search_element_in_list(context.dashboards_scrn.add_function_button())
    context.dashboards_scrn.add_function_button().click()
    context.driver.session(text=context.current_device_name).click()
    context.devices_scrn.search_element_in_list(context.driver.session(text=function))
    context.driver.session(text=function).click()
    context.driver.implicitly_wait()
    assert context.driver.session(text=function).exists(timeout=DEFAULT_TIMEOUT_S)


@when("Remove {function} from the dashboard")
def step_impl(context, function):
    context.driver.session(resourceId=APP_VARIANT + ":id/dashboardsFragment").click()
    context.driver.session(text=function).long_click()
    context.driver.session.implicitly_wait()
    context.driver.session(text="Usuń").click()
    context.driver.implicitly_wait()


@when("Entering the device settings tab")
def step_impl(context):
    context.driver.session(text=context.current_device_name).click()
    context.driver.implicitly_wait()
    clickable_elements = context.driver.session(clickable=True)
    assert len(clickable_elements) >= context.clickable_elements_cnt
    clickable_elements[1].click()  # element[1] stands for settings icon
    context.driver.implicitly_wait()
    page_base_map = {PLUG_NAME: context.plug_scrn, TUYA_GATEWAY_NAME: context.gateway_scrn,
                     TUYA_TRV_NAME: context.trv_scrn, TUYA_SENSOR_NAME: context.sensor_scrn,
                     PM_DETECTOR_NAME: context.pm_detector_scrn, TUYA_CONTACTRON_NAME: context.contactron_scrn,
                     TUYA_POWERSTRIP_NAME: context.powerstrip_scrn}
    page = page_base_map[context.current_device_name]
    page.verifiable_elements = [context.driver.session(text=elem) for elem in
                                        page.verifiable_elements_strings]
    page.check_page_elements()


@when("User adds the device to the room via room settings")
def step_impl(context):
    context.rest_user.delete_device_from_room(USER_ID, SPACE_ID, context.current_room_id, context.current_device_id)
    context.driver.session(resourceId=APP_VARIANT + ":id/zonesFragment").click()
    context.driver.session(text=context.current_device_room).click()
    context.driver.session(resourceId=APP_VARIANT + ":id/right_toolbar_icon").click()
    context.driver.session(text="Dodaj +").click()
    context.driver.session(text=context.current_device_name).click()


@when("User removes device from the room")
def step_impl(context):
    context.driver.session(resourceId=APP_VARIANT + ":id/zonesFragment").click()
    context.driver.session(text=context.current_device_room).click()
    context.driver.session(resourceId=APP_VARIANT + ":id/right_toolbar_icon").click()
    context.driver.implicitly_wait()
    context.driver.session(resourceId=APP_VARIANT + ":id/device_right_button").click()
