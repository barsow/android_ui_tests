from behave import *
from lib.config import APP_VARIANT, USER_PASSWORD
import random
use_step_matcher("re")


@given("Register screen is displayed")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    context.driver.session(resourceId=APP_VARIANT+":id/registrationButton").click()
    context.register_scrn.check_page_elements()


@when("User inserts correct e-mail and passwords")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    context.driver.session(resourceId=APP_VARIANT+":id/emailInput").child(resourceId=APP_VARIANT+":id/input").set_text("automatic_tester"+str(random.randrange(10000))+"@slabs.pl")
    context.driver.session(resourceId=APP_VARIANT+":id/passwordInput").child(resourceId=APP_VARIANT+":id/input").set_text(USER_PASSWORD)
    context.driver.session(resourceId=APP_VARIANT+":id/repeatPasswordInput").child(resourceId=APP_VARIANT+":id/input").set_text(USER_PASSWORD)
    context.driver.session(resourceId=APP_VARIANT+":id/termsCheckboxTick").click()
    context.driver.session(resourceId=APP_VARIANT+":id/registerButton").click()


@then("After-register screen is displayed")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    registration_complete_txt = context.register_scrn.xml_parser.get_name_value('registrationCompleted'
                                                                                '__subtitle_Appartme')
    assert context.driver.session(text=registration_complete_txt).exists(timeout=10)


@when("User inserts e-mail (?P<email>.+) and passwords (?P<password>.+) (?P<confirm_password>.+)")
def step_impl(context, email, password, confirm_password):
    """
    :type context: behave.runner.Context
    :type email: str
    :type password: str
    :type confirm_password: str
    """
    if email == "empty":
        email = ""
    if password == "empty":
        password = ""
    if confirm_password == "empty":
        confirm_password = ""

    context.driver.session(resourceId=APP_VARIANT+":id/emailInput").child(resourceId=APP_VARIANT+":id/input").set_text(email)
    context.driver.session(resourceId=APP_VARIANT+":id/passwordInput").child(resourceId=APP_VARIANT+":id/input").set_text(password)
    context.driver.session(resourceId=APP_VARIANT + ":id/repeatPasswordInput").child(resourceId=APP_VARIANT+":id/input").set_text(confirm_password)
    context.driver.session(resourceId=APP_VARIANT+":id/termsCheckboxTick").click()
    context.driver.session(resourceId=APP_VARIANT+":id/registerButton").click()


@then("Info about different passwords is displayed")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    password_mismatch_txt = context.register_scrn.xml_parser.get_name_value('registration__passwordMismatch_Error')
    assert context.driver.session(text=password_mismatch_txt).exists(timeout=10)
