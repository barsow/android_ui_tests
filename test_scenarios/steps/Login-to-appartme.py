from behave import given, when, then, step
from lib.config import USER_EMAIL, USER_PASSWORD, APP_VARIANT, DEFAULT_TIMEOUT_S


@given("Login screen is displayed")
def step_impl(context):
    context.login_scrn.check_page_elements()


@when("User inserts correct appartme e-mail and password")
def step_impl(context):
    context.driver.session(resourceId=APP_VARIANT + ":id/usernameInput").child(resourceId=APP_VARIANT+":id/input").\
        set_text(context.rest_user.email)
    context.driver.session(resourceId=APP_VARIANT + ":id/passwordInput").child(resourceId=APP_VARIANT+":id/input").\
        set_text(context.rest_user.password)
    context.driver.session(resourceId=APP_VARIANT + ":id/loginButton").click()


@then("Screen with available spaces is displayed")
def step_impl(context):
    available_spaces_txt = context.login_scrn.xml_parser.get_name_value('availableSpaces__title')
    assert(context.driver.session(text=available_spaces_txt).exists(timeout=5))


@then("Info about incorrect e-mail is displayed")
def step_impl(context):
    incorrect_email_txt = context.login_scrn.xml_parser.get_name_value('error__emailIncorrect_Error')
    assert context.driver.device(text=incorrect_email_txt).exists(timeout=DEFAULT_TIMEOUT_S)


@when("User inserts e-mail {email} and password {password}")
def step_impl(context, email, password):
    if email == "empty":
        email = ""
    if password == "empty":
        password = ""
    context.driver.session(resourceId=APP_VARIANT + ":id/usernameInput").child(resourceId=APP_VARIANT+":id/input").set_text(email)
    context.driver.session(resourceId=APP_VARIANT + ":id/passwordInput").child(resourceId=APP_VARIANT+":id/input").set_text(password)
    context.driver.session(resourceId=APP_VARIANT + ":id/loginButton").click()


@then("Info about incorrect password is displayed")
def step_impl(context):
    incorrect_error_txt = context.login_scrn.xml_parser.get_name_value('error__passwordIncorrect_Error')
    assert context.driver.device(text=incorrect_error_txt).exists(timeout=DEFAULT_TIMEOUT_S)


@then("Info about empty field is displayed")
def step_impl(context):
    empty_error_txt = context.login_scrn.xml_parser.get_name_value('error__emptyField_Error')
    assert context.driver.device(text=empty_error_txt).exists(timeout=DEFAULT_TIMEOUT_S)


@when("Wifi is turned off")
def step_impl(context):
    context.driver.turn_off_wifi()


@then("Info about no internet connection is displayed")
def step_impl(context):
    no_internet_txt = context.login_scrn.xml_parser.get_name_value('error__noInternet_Error')
    assert context.driver.device(text=no_internet_txt).exists(timeout=DEFAULT_TIMEOUT_S)
    context.driver.turn_on_wifi()

