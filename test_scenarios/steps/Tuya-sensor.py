from behave import *
from lib.config import *


@given("Preconfigured account with Tuya Sensor")
def step_impl(context):
    context.current_device_name = TUYA_SENSOR_NAME
    context.execute_steps("given Preconfigured tuya account with demanded device")


@when("Add sensor {function} to the dashboard")
def step_impl(context, function):
    context.execute_steps(u'''when Add the {function} of the device to the dashboard'''.format(function=function))


@when("Entering the sensor tab")
def step_impl(context):
    context.execute_steps("when Entering the device tab")


@when("Entering the sensor settings tab")
def step_impl(context):
    context.clickable_elements_cnt = 11
    context.execute_steps("when Entering the device settings tab")


@when("User adds sensor to the room via room settings")
def step_impl(context):
    context.current_room_id = TUYA_SENSOR_ROOM_ID
    context.current_device_id = TUYA_SENSOR_ID
    context.current_device_room = TUYA_SENSOR_ROOM
    context.execute_steps("when User adds the device to the room via room settings")


@when("Changes the sensor name")
def step_impl(context):
    context.new_sensor_name = "New sensor name\n"
    context.sensor_scrn.change_device_name(context.new_sensor_name)


@when("User removes sensor from the room")
def step_impl(context):
    context.current_device_room = TUYA_SENSOR_ROOM
    context.execute_steps("when User removes device from the room")


@step("Sensor and {feature} are not visible")
def step_impl(context, feature):
    context.driver.session(resourceId=APP_VARIANT + ":id/devicesFragment").click()
    assert not context.driver.session(text=TUYA_SENSOR_NAME).exists(timeout=DEFAULT_TIMEOUT_S)
    context.driver.session(resourceId=APP_VARIANT + ":id/dashboardsFragment").click()
    assert not context.driver.session(text=feature).exists(timeout=DEFAULT_TIMEOUT_S)


@then("Sensor is visible in the room settings")
def step_impl(context):
    context.driver.implicitly_wait()
    assert context.driver.session(text=TUYA_SENSOR_NAME).exists(timeout=DEFAULT_TIMEOUT_S)


@then("Sensor is not visible in the room settings")
def step_impl(context):
    context.driver.implicitly_wait()
    assert not context.driver.session(text=TUYA_SENSOR_NAME).exists(timeout=DEFAULT_TIMEOUT_S)


@then("{function} is not visible in the dashboard")
def step_impl(context, function):
    assert not context.driver.session(text=function).exists()


@then("All sensor functions are visible")
def step_impl(context):
    functions_list = ["Temperatura", "Wilgotność", "Bateria", "Temperatura tygodniowa", "Temperatura miesięczna",
                      "Temperatura roczna", "Wilgotność tygodniowa", "Wilgotność miesięczna", "Wilgotność roczna"]
    for feature in functions_list:
        assert context.driver.session(text=feature).exists(timeout=DEFAULT_TIMEOUT_S)
    assert context.devices_scrn.search_element_in_list(context.driver.session(text="Usuń urządzenie")) is True



@then("New sensor name is visible")
def step_impl(context):
    assert context.driver.session(text=context.new_sensor_name).exists(timeout=DEFAULT_TIMEOUT_S)
    context.sensor_scrn.change_device_name(TUYA_SENSOR_NAME)
