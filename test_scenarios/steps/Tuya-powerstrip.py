from behave import *
from lib.config import *


@given("Preconfigured account with Tuya Powerstrip")
def step_impl(context):
    context.current_device_name = TUYA_POWERSTRIP_NAME
    context.execute_steps("given Preconfigured tuya account with demanded device")


@when("Add Powerstrip {function} to the dashboard")
def step_impl(context, function):
    context.execute_steps(u'''when Add the {function} of the device to the dashboard'''.format(function=function))


@when("Entering the Powerstrip tab")
def step_impl(context):
    context.execute_steps("when Entering the device tab")


@when("Changes the Powerstrip name")
def step_impl(context):
    context.new_powerstrip_name = "New Powerstrip name\n"
    context.powerstrip_scrn.change_device_name(context.new_powerstrip_name)


@when("Entering the Powerstrip settings tab")
def step_impl(context):
    context.clickable_elements_cnt = 9
    context.execute_steps("when Entering the device settings tab")


@when("User adds Powerstrip to the room via room settings")
def step_impl(context):
    context.current_room_id = TUYA_POWERSTRIP_ROOM_ID
    context.current_device_id = TUYA_POWERSTRIP_ID
    context.current_device_room = TUYA_POWERSTRIP_ROOM
    context.execute_steps("when User adds the device to the room via room settings")


@when("User removes Powerstrip from the room")
def step_impl(context):
    context.current_device_room = TUYA_POWERSTRIP_ROOM
    context.execute_steps("when User removes device from the room")


@then("All Powerstrip functions are visible")
def step_impl(context):
    functions_list = ["Zasilanie nr 1", "Zasilanie nr 2", "Zasilanie nr 3", "Wejścia USB", "Nadchodzące zdarzenie",
                      "Wszystkie przełączniki"]
    for feature in functions_list:
        assert context.driver.session(text=feature).exists(timeout=DEFAULT_TIMEOUT_S)


@then("New Powerstrip name is visible")
def step_impl(context):
    assert context.driver.session(text=context.new_powerstrip_name).exists(timeout=DEFAULT_TIMEOUT_S)
    context.powerstrip_scrn.change_device_name(TUYA_POWERSTRIP_NAME)


@then("Powerstrip is visible in the room settings")
def step_impl(context):
    context.driver.implicitly_wait()
    assert context.driver.session(text=TUYA_POWERSTRIP_NAME).exists(timeout=DEFAULT_TIMEOUT_S)


@then("Powerstrip is not visible in the room settings")
def step_impl(context):
    context.driver.implicitly_wait()
    assert not context.driver.session(text=TUYA_POWERSTRIP_NAME).exists(timeout=DEFAULT_TIMEOUT_S)
