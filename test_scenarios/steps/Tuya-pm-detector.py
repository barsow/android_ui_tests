from behave import *
from lib.config import *
use_step_matcher("re")


@given("Preconfigured account with PM detector")
def step_impl(context):
    context.current_device_name = PM_DETECTOR_NAME
    context.execute_steps("given Preconfigured tuya account with demanded device")


@when("User adds PM detector to the room via room settings")
def step_impl(context):
    context.driver.session(resourceId=APP_VARIANT + ":id/zonesFragment").click()
    context.driver.session(text=PM_DETECTOR_ROOM).click()
    context.driver.session(resourceId=APP_VARIANT + ":id/right_toolbar_icon").click()
    context.driver.session(text="Dodaj +").click() #not in strings.xml
    context.driver.session(text=PM_DETECTOR_NAME).click()


@when("User adds PM detector (?P<feature>.+) to the dashboard")
def step_impl(context, feature):
    context.execute_steps(u'''when Add the {function} of the device to the dashboard'''.format(function=feature))


@when("User removes PM detector (?P<feature>.+) from the dashboard")
def step_impl(context, feature):
    context.execute_steps(u'''when Remove {function} from the dashboard'''.format(function=feature))


@then("PM detector is visible in the room settings")
def step_impl(context):
    context.driver.implicitly_wait()
    assert context.driver.session(text=PM_DETECTOR_ROOM).exists(timeout=DEFAULT_TIMEOUT_S)


@when("User removes PM detector from the room")
def step_impl(context):
    context.current_device_room = PM_DETECTOR_ROOM
    context.execute_steps("when User removes device from the room")


@then("PM detector is not visible in the room settings")
def step_impl(context):
    context.driver.implicitly_wait()
    assert not context.driver.session(text=PM_DETECTOR_NAME).exists(timeout=DEFAULT_TIMEOUT_S)


@when("PM detector and (?P<feature>.+) are not visible")
def step_impl(context, feature):
    context.driver.session(resourceId=APP_VARIANT + ":id/devicesFragment").click()
    assert not context.driver.session(text=PM_DETECTOR_NAME).exists(timeout=DEFAULT_TIMEOUT_S)
    context.driver.session(resourceId=APP_VARIANT + ":id/dashboardsFragment").click()
    assert not context.driver.session(text=feature).exists(timeout=DEFAULT_TIMEOUT_S)


@when("Entering the PM detector tab")
def step_impl(context):
    context.driver.session(text=PM_DETECTOR_NAME).click()
    context.driver.implicitly_wait()


@when("Entering the PM detector settings tab")
def step_impl(context):
    context.clickable_elements_cnt = 11
    context.execute_steps("when Entering the device settings tab")


@then("All PM detector functions are visible")
def step_impl(context):
    functions_list = ["Stężenie PM1", "Stężenie PM2.5", "Stężenie PM10", "Temperatura", "Wilgotność", "Bateria"]
    for feature in functions_list:
        assert context.driver.session(text=feature).exists(timeout=DEFAULT_TIMEOUT_S)
        print(feature)


@step("Changes the PM detector name")
def step_impl(context):
    context.new_pm_detector_name = "New pm name\n"
    context.sensor_scrn.change_device_name(context.new_pm_detector_name)


@then("New PM detector name is visible")
def step_impl(context):
    assert context.driver.session(text=context.new_pm_detector_name).exists(timeout=DEFAULT_TIMEOUT_S)
    context.sensor_scrn.change_device_name(PM_DETECTOR_NAME)
