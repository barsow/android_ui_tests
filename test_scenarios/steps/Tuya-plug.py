from behave import *
from lib.config import *
use_step_matcher("re")


@given("Preconfigured account with Tuya Plug")
def step_impl(context):
    context.current_device_name = PLUG_NAME
    context.execute_steps("given Preconfigured tuya account with demanded device")


@when("User adds plug (?P<feature>.+) to the dashboard")
def step_impl(context, feature):
    context.execute_steps(u'''when Add the {function} of the device to the dashboard'''.format(function=feature))


@when("User removes plug (?P<feature>.+) from the dashboard")
def step_impl(context, feature):
    context.execute_steps(u'''when Remove {function} from the dashboard'''.format(function=feature))


@then("(?P<feature>.+) is not visible at the dashboard")
def step_impl(context, feature):
    assert not context.driver.session(text=feature).exists(timeout=DEFAULT_TIMEOUT_S)


@when("User adds plug to the room via room settings")
def step_impl(context):
    context.driver.session(resourceId=APP_VARIANT + ":id/zonesFragment").click()
    context.driver.session(text=PLUG_ROOM).click()
    context.driver.session(resourceId=APP_VARIANT + ":id/right_toolbar_icon").click()
    context.driver.session(text="Dodaj +").click() #not in strings.xml
    context.driver.session(text=PLUG_NAME).click()


@then("Plug is visible in the room settings")
def step_impl(context):
    context.driver.implicitly_wait()
    assert context.driver.session(text=PLUG_NAME).exists(timeout=DEFAULT_TIMEOUT_S)


@when("User removes plug from the room")
def step_impl(context):
    context.current_device_room = PLUG_ROOM
    context.execute_steps("when User removes device from the room")


@then("Plug is not visible in the room settings")
def step_impl(context):
    context.driver.implicitly_wait()
    assert not context.driver.session(text=PLUG_NAME).exists(timeout=DEFAULT_TIMEOUT_S)

@when("Entering the plug tab")
def step_impl(context):
    context.driver.session(text=PLUG_NAME).click()
    context.driver.implicitly_wait()


@when("Entering the plug settings tab")
def step_impl(context):
    context.clickable_elements_cnt = 11
    context.execute_steps("when Entering the device settings tab")


@then("All plug functions are visible")
def step_impl(context):
    functions_list = ["Zasilanie", "Zużycie tygodniowe", "Zużycie miesięczne", "Zużycie roczne", "Natężenie prądu",
                      "Pobór energii", "Napięcie", "Nadchodzące zdarzenie"]
    for feature in functions_list:
        assert context.driver.session(text=feature).exists(timeout=DEFAULT_TIMEOUT_S)
        print(feature)


@step("Changes the plug name")
def step_impl(context):
    context.new_plug_name = "New plug name\n"
    context.sensor_scrn.change_device_name(context.new_plug_name)


@then("New plug name is visible")
def step_impl(context):
    assert context.driver.session(text=context.new_plug_name).exists(timeout=DEFAULT_TIMEOUT_S)
    context.sensor_scrn.change_device_name(PLUG_NAME)


@step("Plug and (?P<feature>.+) are not visible")
def step_impl(context, feature):
    context.driver.session(resourceId=APP_VARIANT + ":id/devicesFragment").click()
    assert not context.driver.session(text=PLUG_NAME).exists(timeout=DEFAULT_TIMEOUT_S)
    context.driver.session(resourceId=APP_VARIANT + ":id/dashboardsFragment").click()
    assert not context.driver.session(text=feature).exists(timeout=DEFAULT_TIMEOUT_S)
