from behave import *
from lib.config import APP_VARIANT
from lib.config import DEFAULT_TIMEOUT_S

use_step_matcher("re")


@given("Tuya register screen is displayed")
def step_impl(context):
    context.driver.session(resourceId=APP_VARIANT + ":id/usernameInput").child(
        resourceId=APP_VARIANT + ":id/input").set_text(context.rest_user.email)
    context.driver.session(resourceId=APP_VARIANT + ":id/passwordInput").child(
        resourceId=APP_VARIANT + ":id/input").set_text(context.rest_user.password)
    context.driver.session(resourceId=APP_VARIANT + ":id/loginButton").click()
    context.driver.session(text=context.space_name).click()
    context.driver.session(resourceId=APP_VARIANT+":id/accountFragment").click()
    context.driver.implicitly_wait()
    context.driver.device.swipe(0.5, 0.7, 0.5, 0.5)
    context.driver.device.swipe(0.5, 0.7, 0.5, 0.5)
    try:
        context.driver.session(resourceId=APP_VARIANT + ":id/rightImage")[1].click()
    except:
        context.driver.session(resourceId=APP_VARIANT + ":id/rightImage").click()
    context.register_scrn.search_element_in_list(context.driver.
                                                 session(resourceId=APP_VARIANT+":id/registrationButton"))
    context.driver.session(resourceId=APP_VARIANT+":id/registrationButton").click()
    context.register_scrn.check_page_elements()


@then("Tuya after-register screen is displayed")
def step_impl(context):
    registration_completed_txt = context.login_scrn.xml_parser.get_name_value('registrationCompleted__subtitle_Tuya')
    assert context.driver.session(text=registration_completed_txt).exists(timeout=DEFAULT_TIMEOUT_S)
