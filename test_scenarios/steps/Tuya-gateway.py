from behave import *
from lib.config import *


@given("Preconfigured account with Tuya Gateway")
def step_impl(context):
    context.current_device_name = TUYA_GATEWAY_NAME
    context.execute_steps("given Preconfigured tuya account with demanded device")


@when("Entering the gateway tab")
def step_impl(context):
    context.execute_steps("when Entering the device tab")


@when("Entering the gateway settings")
def step_impl(context):
    clickable_elements = context.driver.session(clickable=True)
    assert len(clickable_elements) >= 3
    clickable_elements[1].click()  # element[1] stands for settings icon
    context.driver.implicitly_wait()


@when("User adds gateway to the room via room settings")
def step_impl(context):
    context.current_room_id = TUYA_SENSOR_ROOM_ID
    context.current_device_id = TUYA_SENSOR_ID
    context.current_device_room = TUYA_SENSOR_ROOM
    context.execute_steps("when User adds the device to the room via room settings")


@when("User removes gateway from the room")
def step_impl(context):
    context.current_device_room = TUYA_SENSOR_ROOM
    context.execute_steps("when User removes device from the room")


@then("All gateway settings are visible")
def step_impl(context):
    context.gateway_scrn.verifiable_elements = [context.driver.session(text=elem) for elem in
                                               context.gateway_scrn.verifiable_elements_strings]
    context.gateway_scrn.check_page_elements()


@then("Gateway is visible in the room settings")
def step_impl(context):
    context.driver.implicitly_wait()
    assert context.driver.session(text=TUYA_GATEWAY_NAME).exists(timeout=DEFAULT_TIMEOUT_S)


@then("Gateway is not visible in the room settings")
def step_impl(context):
    context.driver.implicitly_wait()
    assert not context.driver.session(text=TUYA_GATEWAY_NAME).exists(timeout=DEFAULT_TIMEOUT_S)
    context.driver.session(text="Dodaj +").click()
    context.driver.session(text=TUYA_GATEWAY_NAME).click()
