from behave import *
from lib.config import USER_PASSWORD, USER_EMAIL, APP_VARIANT, DEFAULT_TIMEOUT_S
import time


@given("Space screen after login is displayed")
def step_impl(context):
    context.driver.session(resourceId=APP_VARIANT + ":id/usernameInput").child(
        resourceId=APP_VARIANT + ":id/input").set_text(context.rest_user.email)
    context.driver.session(resourceId=APP_VARIANT + ":id/passwordInput").child(
        resourceId=APP_VARIANT + ":id/input").set_text(context.rest_user.password)
    context.driver.session(resourceId=APP_VARIANT + ":id/loginButton").click()
    context.spaces_scrn.check_page_elements()


@when("User clicks new space button on spaces screen")
def step_impl(context):
    context.driver.implicitly_wait()
    create_button_txt = context.login_scrn.xml_parser.get_name_value('availableSpaces__createButton')
    context.login_scrn.search_element_in_list(context.driver.session(text=create_button_txt))
    context.driver.session(text=create_button_txt).click()


@when("User clicks new space button on accounts screen")
def step_impl(context):
    context.driver.implicitly_wait()
    create_button_txt = context.login_scrn.xml_parser.get_name_value('createSpace__title')
    context.driver.session(text=create_button_txt).click()


@step('User inserts correct space name {space_name}')
def step_impl(context, space_name):
    new_space_button_txt = context.login_scrn.xml_parser.get_name_value('accountTab__newSpaceButton')
    context.driver.session(resourceId=APP_VARIANT+":id/nameInput").child(resourceId=APP_VARIANT+":id/input").set_text(space_name)
    context.driver.session(text=new_space_button_txt).click()


@then("{space_name} space is in the Accounts tab")
def step_impl(context, space_name):
    context.driver.implicitly_wait()
    context.driver.session(resourceId=APP_VARIANT+":id/accountFragment").click()
    assert(context.driver.session(text=space_name).exists(timeout=DEFAULT_TIMEOUT_S))


@step("User chooses space {space_name}")
def step_impl(context, space_name):
    context.driver.implicitly_wait()
    context.driver.session(text=space_name).click()


@given("Accounts tab is displayed for space {space_name}")
def step_impl(context, space_name):
    context.driver.session(resourceId=APP_VARIANT + ":id/usernameInput").child(
        resourceId=APP_VARIANT + ":id/input").set_text(context.rest_user.email)
    context.driver.session(resourceId=APP_VARIANT + ":id/passwordInput").child(
        resourceId=APP_VARIANT + ":id/input").set_text(context.rest_user.password)
    context.driver.session(resourceId=APP_VARIANT + ":id/loginButton").click()
    time.sleep(3)
    context.login_scrn.search_element_in_list(context.driver.session(text=space_name))
    context.driver.session(text=space_name).click()
    context.driver.session(resourceId=APP_VARIANT+":id/accountFragment").click()


@when("User clicks button")
def step_impl(context):
    context.driver.session(resourceId=APP_VARIANT+":id/button").click()


@when("User deletes spaces {space_name1} {space_name2}")
def step_impl(context, space_name1, space_name2):
    context.driver.implicitly_wait()
    delete_confirm_button_txt = context.login_scrn.xml_parser.get_name_value('editSpace__deleteConfirmButton').upper()
    context.driver.session(resourceId=APP_VARIANT+":id/rightImage").click()
    context.driver.session(resourceId=APP_VARIANT+":id/deleteButton").click()
    context.driver.session(text=delete_confirm_button_txt).click()
    context.driver.session(text=space_name2).click()
    context.driver.session(resourceId=APP_VARIANT+":id/accountFragment").click()
    context.driver.implicitly_wait()
    context.driver.session(resourceId=APP_VARIANT+":id/rightImage").click()
    context.driver.session(resourceId=APP_VARIANT+":id/deleteButton").click()
    context.driver.session(text=delete_confirm_button_txt).click()
    context.driver.implicitly_wait()


@then("Screen with no {space_name1} or {space_name2} is displayed")
def step_impl(context, space_name1, space_name2):
    context.spaces_scrn.check_page_elements()
    assert not context.driver.session(text=space_name1).exists(timeout=DEFAULT_TIMEOUT_S)
    assert not context.driver.session(text=space_name2).exists(timeout=DEFAULT_TIMEOUT_S)


@when("User changes space name to {new_space_name}")
def step_impl(context, new_space_name):
    context.driver.implicitly_wait()
    context.driver.session(resourceId=APP_VARIANT+":id/rightImage").click()
    context.driver.implicitly_wait()
    context.driver.session(resourceId=APP_VARIANT+":id/nameInput").child(resourceId=APP_VARIANT+":id/input").set_text(new_space_name)
    context.driver.session(resourceId=APP_VARIANT+":id/button").click()


@step("User inserts incorrect space name {space_name}")
def step_impl(context, space_name):
    context.driver.implicitly_wait()
    if space_name=='empty':
        space_name=""
    elif space_name=='only_whitespace':
        space_name=" "
    context.driver.session(resourceId=APP_VARIANT+":id/nameInput").child(resourceId=APP_VARIANT+":id/input").set_text(space_name)
    context.driver.session(resourceId=APP_VARIANT+":id/button").click()


@step("User edits name to incorrect space name {space_name}")
def step_impl(context, space_name):
    context.driver.implicitly_wait()
    context.driver.session(resourceId=APP_VARIANT+":id/rightImage").click()
    context.driver.implicitly_wait()
    if space_name=='empty':
        space_name=""
    elif space_name=='only_whitespace':
        space_name=" "
    context.driver.session(resourceId=APP_VARIANT+":id/nameInput").child(resourceId=APP_VARIANT+":id/input").set_text(space_name)
    context.driver.session(resourceId=APP_VARIANT+":id/button").click()


@then("Info about incorrect space name is displayed")
def step_impl(context):
    assert context.driver.session(text="400 ServerError").exists(timeout=DEFAULT_TIMEOUT_S)
    # TODO change to valid error when its added
