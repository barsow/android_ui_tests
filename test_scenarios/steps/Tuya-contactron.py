from behave import *
from lib.config import *


@Given("Preconfigured account with Tuya Contactron")
def step_impl(context):
    context.current_device_name = TUYA_CONTACTRON_NAME
    context.execute_steps("given Preconfigured tuya account with demanded device")


@when("Entering the contactron tab")
def step_impl(context):
    context.execute_steps("when Entering the device tab")


@when("Changes the contactron name")
def step_impl(context):
    context.new_contactron_name = "Kontak tron\n"
    context.contactron_scrn.change_device_name(context.new_contactron_name)


@when("Add contactron {function} to the dashboard")
def step_impl(context, function):
    context.execute_steps(u'''when Add the {function} of the device to the dashboard'''.format(function=function))


@when("User adds contactron to the room via room settings")
def step_impl(context):
    context.current_room_id = TUYA_CONTACTRON_ROOM_ID
    context.current_device_id = TUYA_CONTACTRON_ID
    context.current_device_room = TUYA_CONTACTRON_ROOM
    context.execute_steps("when User adds the device to the room via room settings")


@when("User removes contactron from the room")
def step_impl(context):
    context.current_device_room = TUYA_CONTACTRON_ROOM
    context.execute_steps("when User removes device from the room")


@then("All contactron functions are visible")
def step_impl(context):
    functions_list = ["Czujnik otwarcia", "Bateria"]
    for feature in functions_list:
        assert context.driver.session(text=feature).exists(timeout=DEFAULT_TIMEOUT_S)


@then("New contactron name is visible")
def step_impl(context):
    assert context.driver.session(text=context.new_contactron_name).exists(timeout=DEFAULT_TIMEOUT_S)
    context.contactron_scrn.change_device_name(TUYA_CONTACTRON_NAME)


@when("Entering the contactron settings tab")
def step_impl(context):
    context.clickable_elements_cnt = 5
    context.execute_steps("when Entering the device settings tab")


@then("Contactron is visible in the room settings")
def step_impl(context):
    context.driver.implicitly_wait()
    assert context.driver.session(text=TUYA_CONTACTRON_NAME).exists(timeout=DEFAULT_TIMEOUT_S)


@then("Contactron is not visible in the room settings")
def step_impl(context):
    context.driver.implicitly_wait()
    assert not context.driver.session(text=TUYA_CONTACTRON_NAME).exists(timeout=DEFAULT_TIMEOUT_S)
