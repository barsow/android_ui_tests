from behave import *
from lib.config import APP_VARIANT, DEFAULT_TIMEOUT_S


@given("Account tab after login is displayed")
def step_impl(context):
    context.login_scrn.login_user(context.rest_user.email, context.rest_user.password)
    space_elem = context.driver.session(text=context.space_name)
    space_elem.exists(timeout=5)
    space_elem.click()


@given("User selects rooms tab")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    context.rooms_scrn.rooms_tab().click()
    context.rooms_scrn.check_page_elements()


@when("User clicks new room button")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    context.driver.implicitly_wait()
    context.rooms_scrn.new_room_button().click()


@when("User inserts room name")
def step_impl(context):
    create_button_txt = context.login_scrn.xml_parser.get_name_value('common__create')
    context.room_name = 'New room'
    context.rooms_scrn.input_field().set_text(context.room_name)
    context.driver.session(text=create_button_txt).click()


@step("User inserts incorrect room name {room_name}")
def step_impl(context, room_name):
    context.driver.implicitly_wait()
    if room_name == 'empty':
        room_name = ""
    elif room_name == 'only_whitespace':
        room_name = " "
    context.rooms_scrn.input_field().set_text(room_name)
    context.driver.session(resourceId=APP_VARIANT+":id/button").click()


@when("User clicks a room picture")
def step_impl(context):
    context.room_name = context.rest_user.get_user_zones(context.rest_user.user_id, context.space_id).json()['response'][0]['name']
    context.driver.implicitly_wait()
    context.driver.session(text=context.room_name).click()


@when("User selects settings")
def step_impl(context):
    context.rooms_scrn.toolbar_icon().click()


@when("User changes the room name")
def step_impl(context):
    context.rooms_scrn.change_name_button().click()
    context.room_name = 'Edited n3w room name!'
    context.driver.device(resourceId=APP_VARIANT + ":id/input").set_text(context.room_name)
    context.rooms_scrn.save_button().click()


@when("User removes the room")
def step_impl(context):
    context.rooms_scrn.remove_button().click()
    context.driver.device(resourceId=APP_VARIANT + ":id/input").set_text(context.room_name)
    context.driver.session(text='Usuń').click()
    context.driver.implicitly_wait()


@then("Room with new name is visible")
def step_impl(context):
    assert (context.driver.session(text=context.room_name).exists(timeout=5))
    context.driver.press_system_button("back")
    assert (context.driver.session(text=context.room_name).exists(timeout=5))
    context.driver.press_system_button("back")
    assert (context.driver.session(text=context.room_name).exists(timeout=5))


@then("Room is not visible")
def step_impl(context):
    assert(context.driver.session(text=context.room_name).exists(timeout=5) is False)
    context.driver.press_system_button("back")
    assert(context.driver.session(text=context.room_name).exists(timeout=5) is False)
    context.driver.press_system_button("back")
    assert(context.driver.session(text=context.room_name).exists(timeout=5) is False)


@then("New room is in the rooms tab")
def step_impl(context):
    context.rooms_scrn.rooms_tab().click()
    context.driver.implicitly_wait()
    assert (context.driver.session(text=context.room_name).exists(timeout=DEFAULT_TIMEOUT_S))


@then("Info about incorrect name is displayed")
def step_impl(context):
    assert context.driver.session(text="400 ServerError").exists(timeout=DEFAULT_TIMEOUT_S)
    #TODO change to valid error when its added