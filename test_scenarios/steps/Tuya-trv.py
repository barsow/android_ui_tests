from behave import *
from lib.config import *


@given("Preconfigured account with Tuya TRV")
def step_impl(context):
    context.current_device_name = TUYA_TRV_NAME
    context.execute_steps("given Preconfigured tuya account with demanded device")


@when("Add trv {function} to the dashboard")
def step_impl(context, function):
    context.execute_steps(u'''when Add the {function} of the device to the dashboard'''.format(function=function))


@when("Entering the TRV tab")
def step_impl(context):
    context.execute_steps("when Entering the device tab")


@when("Changes the TRV name")
def step_impl(context):
    context.new_trv_name = "New TRV name\n"
    context.sensor_scrn.change_device_name(context.new_trv_name)


@when("Entering the TRV settings tab")
def step_impl(context):
    context.clickable_elements_cnt = 7
    context.execute_steps("when Entering the device settings tab")


@when("User adds TRV to the room via room settings")
def step_impl(context):
    context.current_room_id = TUYA_TRV_ROOM_ID
    context.current_device_id = TUYA_TRV_ID
    context.current_device_room = TUYA_TRV_ROOM
    context.execute_steps("when User adds the device to the room via room settings")


@when("User removes TRV from the room")
def step_impl(context):
    context.current_device_room = TUYA_TRV_ROOM
    context.execute_steps("when User removes device from the room")


@then("All TRV functions are visible")
def step_impl(context):
    functions_list = ["Temperatura", "Ustawiona temperatura", "Bateria", "Nadchodzące zdarzenie"]
    for feature in functions_list:
        assert context.driver.session(text=feature).exists(timeout=DEFAULT_TIMEOUT_S)


@then("New TRV name is visible")
def step_impl(context):
    assert context.driver.session(text=context.new_trv_name).exists(timeout=DEFAULT_TIMEOUT_S)
    context.trv_scrn.change_device_name(TUYA_TRV_NAME)


@then("TRV is visible in the room settings")
def step_impl(context):
    context.driver.implicitly_wait()
    assert context.driver.session(text=TUYA_TRV_NAME).exists(timeout=DEFAULT_TIMEOUT_S)


@then("TRV is not visible in the room settings")
def step_impl(context):
    context.driver.implicitly_wait()
    assert not context.driver.session(text=TUYA_TRV_NAME).exists(timeout=DEFAULT_TIMEOUT_S)
