from behave import *
from lib.config import APP_VARIANT, DEFAULT_TIMEOUT_S
from lib.helpers import generate_random_name


@given("Configured user account opened on dashboard tab")
def step_impl(context):
    context.driver.session(resourceId=APP_VARIANT + ":id/usernameInput").child(
        resourceId=APP_VARIANT + ":id/input").set_text(context.rest_user.email)
    context.driver.session(resourceId=APP_VARIANT + ":id/passwordInput").child(
        resourceId=APP_VARIANT + ":id/input").set_text(context.rest_user.password)
    context.driver.session(resourceId=APP_VARIANT + ":id/loginButton").click()
    context.driver.session(text=context.space_name).click()
    context.dashboards_scrn.check_page_elements()


@given("Configured user account with a dashboard opened")
def step_impl(context):
    context.driver.session(resourceId=APP_VARIANT + ":id/usernameInput").child(
        resourceId=APP_VARIANT + ":id/input").set_text(context.rest_user.email)
    context.driver.session(resourceId=APP_VARIANT + ":id/passwordInput").child(
        resourceId=APP_VARIANT + ":id/input").set_text(context.rest_user.password)
    response_dashboard = context.rest_user.create_dashboard(context.rest_user.user_id,
                                                            context.space_id)
    assert response_dashboard.status_code == 200
    context.dashboard_name = response_dashboard.json()['response']['name']
    context.driver.session(resourceId=APP_VARIANT + ":id/loginButton").click()
    context.dashboards_scrn.search_element_in_list(context.driver.session(text=context.space_name))
    context.driver.session(text=context.space_name).click()
    context.driver.session(resourceId=APP_VARIANT + ':id/rightImageView').click()


@when("User clicks new dashboard button")
def step_impl(context):
    context.dashboards_scrn.add_button().click()
    context.driver.implicitly_wait()


@when("User deletes a dashboard")
def step_impl(context):
    context.dashboards_scrn.delete_dashboard(context.dashboard_name)


@when("User changes dashboard name to {dashboard_name}")
def step_impl(context, dashboard_name):
    context.dashboards_scrn.edit_dashboard(dashboard_name)


@when("User edits dashboard name to {incorrect}")
def step_impl(context, incorrect):
    context.too_long_name = generate_random_name(False, 35)
    incorrectness_dict = {'empty': '', 'only_whitespace': '       ',
                          'too_long': context.too_long_name}
    context.dashboards_scrn.edit_dashboard(incorrectness_dict[incorrect])


@step('User inserts correct dashboard name {dashboard_name}')
def step_impl(context, dashboard_name):
    context.dashboards_scrn.add_dashboard(dashboard_name)


@then("{dashboard_name} is in the dashboards settings screen")
def step_impl(context, dashboard_name):
    context.driver.implicitly_wait()
    assert(context.driver.session(text=dashboard_name).exists(timeout=DEFAULT_TIMEOUT_S))


@then("Screen with no dashboard is displayed")
def step_impl(context):
    context.driver.implicitly_wait()
    assert(context.driver.session(text=context.dashboard_name).exists() is False)


@then("Approriate action for {incorrect} name is performed")
def step_impl(context, incorrect):
    if incorrect in ['empty', 'only_whitespace']:
        context.dashboards_scrn.verify_edit_dashboard_dialog()
    elif incorrect == 'too_long':
        context.dashboards_scrn.verify_dashboard_name_trimmed(context.too_long_name)
