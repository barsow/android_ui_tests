from behave import *
from lib.config import APP_VARIANT, DEFAULT_TIMEOUT_S, TUYA_EMAIL, TUYA_PASSWORD


@given("Tuya Login screen is displayed")
def step_impl(context):
    context.driver.session(resourceId=APP_VARIANT + ":id/usernameInput").child(
        resourceId=APP_VARIANT + ":id/input").set_text(context.rest_user.email)
    context.driver.session(resourceId=APP_VARIANT + ":id/passwordInput").child(
        resourceId=APP_VARIANT + ":id/input").set_text(context.rest_user.password)
    context.driver.session(resourceId=APP_VARIANT + ":id/loginButton").click()
    context.driver.session(text=context.space_name).click()
    context.driver.session(resourceId=APP_VARIANT+":id/accountFragment").click()
    context.driver.device.swipe(0.5, 0.7, 0.5, 0.5)
    context.driver.device.swipe(0.5, 0.7, 0.5, 0.5)
    context.register_scrn.search_element_in_list(context.driver.session(resourceId=APP_VARIANT + ":id/rightImage"))
    try:
        context.driver.session(resourceId=APP_VARIANT + ":id/rightImage")[1].click()
    except:
        context.driver.session(resourceId=APP_VARIANT + ":id/rightImage").click()
    context.login_scrn.check_page_elements()


@then("Empty dashboard screen is displayed")
def step_impl(context):
    response_dashboard = context.rest_user.get_user_dashboards(context.rest_user.user_id, context.space_id)
    dashboards = len(response_dashboard.json()['response'])
    if dashboards:
        dashboard_txt = context.login_scrn.xml_parser.get_name_value('dashboardsTab__functionsEmptyState')
    else:
        dashboard_txt = context.login_scrn.xml_parser.get_name_value('dashboardsTab__dashboardsEmptyState')
    assert context.driver.session(text=dashboard_txt).exists(timeout=DEFAULT_TIMEOUT_S)


@step("User logouts from tuya")
def step_impl(context):
    yes_txt = context.login_scrn.xml_parser.get_name_value('common__yes').upper()
    context.driver.session(resourceId=APP_VARIANT+":id/accountFragment").click()
    context.driver.session(resourceId=APP_VARIANT + ":id/rightImage")[1].click()
    context.driver.session(text=yes_txt).click()


@then("User is logged out")
def step_impl(context):
    logged_out_txt = context.login_scrn.xml_parser.get_name_value('accountTab__accountStatus_LoggedOut')
    assert context.driver.session(text=logged_out_txt).exists(timeout=DEFAULT_TIMEOUT_S)


@when("User inserts correct tuya e-mail and password")
def step_impl(context):
    context.driver.session(resourceId=APP_VARIANT + ":id/usernameInput").child(resourceId=APP_VARIANT+":id/input").\
        set_text(TUYA_EMAIL)
    context.driver.session(resourceId=APP_VARIANT + ":id/passwordInput").child(resourceId=APP_VARIANT+":id/input").\
        set_text(TUYA_PASSWORD)
    context.driver.session(resourceId=APP_VARIANT + ":id/loginButton").click()


@step("User login to tuya")
def step_impl(context):
    context.driver.session(resourceId=APP_VARIANT+":id/accountFragment").click()
    context.driver.session(resourceId=APP_VARIANT + ":id/rightImage")[1].click()
    context.login_scrn.check_page_elements()
    context.driver.session(resourceId=APP_VARIANT + ":id/usernameInput").child(resourceId=APP_VARIANT+":id/input").\
        set_text(TUYA_EMAIL)
    context.driver.session(resourceId=APP_VARIANT + ":id/passwordInput").child(resourceId=APP_VARIANT+":id/input").\
        set_text(TUYA_PASSWORD)
    context.driver.session(resourceId=APP_VARIANT + ":id/loginButton").click()
