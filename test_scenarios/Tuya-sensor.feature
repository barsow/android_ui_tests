Feature: Tuya Sensor

  Scenario: Sensor includes all needed functions
  Given Preconfigured account with Tuya Sensor
    When Entering the sensor tab
    Then All sensor functions are visible

  Scenario: Sensor settings tab and name change work correctly
  Given Preconfigured account with Tuya Sensor
    When Entering the sensor settings tab
    And Changes the sensor name
    Then New sensor name is visible

  Scenario Outline: Add and remove sensor features from the dashboard
    Given Preconfigured account with Tuya Sensor
    When Add sensor <function> to the dashboard
    And Remove <function> from the dashboard
    Then <function> is not visible in the dashboard

    Examples:
    |             function                    |
    |             Temperatura                 |
    |             Wilgotność                  |
    |             Bateria                     |
    |             Temperatura tygodniowa      |
    |             Temperatura miesięczna      |
    |             Temperatura roczna          |
    |             Wilgotność tygodniowa       |
    |             Wilgotność miesięczna       |
    |             Wilgotność roczna           |

  Scenario: Add sensor to the room
    Given Preconfigured account with Tuya Sensor
    When User adds sensor to the room via room settings
    Then Sensor is visible in the room settings

  Scenario: Remove sensor from the room
    Given Preconfigured account with Tuya Sensor
    When User removes sensor from the room
    Then Sensor is not visible in the room settings

  Scenario Outline: Logout from tuya
    Given Preconfigured account with Tuya Sensor
    When Add sensor <feature> to the dashboard
    And User logouts from tuya
    And Sensor and <feature> are not visible
    And User login to tuya
    And Remove <feature> from the dashboard
    Then <feature> is not visible at the dashboard

    Examples:
    |           feature               |
    |           Wilgotność miesięczna |
    |           Wilgotność roczna     |
