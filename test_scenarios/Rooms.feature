Feature: Rooms

  Scenario: Creating first room
    Given Account tab after login is displayed
    And User selects rooms tab
    When User clicks new room button
    And User inserts room name
    Then New room is in the rooms tab

  Scenario Outline: Creating room with incorrect name
    Given Account tab after login is displayed
    And User selects rooms tab
    When User clicks new room button
    And User inserts incorrect room name <room_name>
    Then Info about incorrect name is displayed
    Examples: Users
      |  room_name     |
      |  empty          |
      |  only_whitespace|

  Scenario: Creating room with no internet connection
    Given Account tab after login is displayed
    And User selects rooms tab
    When User clicks new room button
    And Wifi is turned off
    And User inserts room name
    Then Info about no internet connection is displayed

#  Scenario: Changing room name
#    Given Account tab after login is displayed
#    And User selects rooms tab
#    When User clicks a room picture
#    And User selects settings
#    And User changes the room name
#    Then Room with new name is visible
#
#  Scenario: Removing a room
#    Given Account tab after login is displayed
#    And User selects rooms tab
#    When User clicks a room picture
#    And User selects settings
#    And User removes the room
#    Then Room is not visible
