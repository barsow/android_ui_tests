Feature: PM detector
#
#  Scenario: PM detector includes all needed functions
#    Given Preconfigured account with PM detector
#    When Entering the PM detector tab
#    Then All PM detector functions are visible
#
#  Scenario: Add PM detector to the room
#    Given Preconfigured account with PM detector
#    When User adds PM detector to the room via room settings
#    Then PM detector is visible in the room settings
#
#  Scenario: PM detector settings tab and name change work correctly
#    Given Preconfigured account with PM detector
#    When Entering the PM detector settings tab
#    And Changes the PM detector name
#    Then New PM detector name is visible
#
#  Scenario: Remove PM detector from the room
#    Given Preconfigured account with PM detector
#    When User removes PM detector from the room
#    Then PM detector is not visible in the room settings
#
#  Scenario Outline: Add and remove PM detector features from the dashboard
#    Given Preconfigured account with PM detector
#    When User adds PM detector <feature> to the dashboard
#    And User removes PM detector <feature> from the dashboard
#    Then <feature> is not visible at the dashboard
#
#    Examples:
#    |             feature                     |
#    |          Stężenie PM1                   |
#    |          Stężenie PM2.5                 |
#    |          Stężenie PM10                  |
#    |          Temperatura                    |
#    |          Wilgotność                     |
#    |          Bateria                        |
#
#
#  Scenario Outline: Logout from tuya
#    Given Preconfigured account with PM detector
#    When User adds PM detector <feature> to the dashboard
#    And User logouts from tuya
#    And PM detector and <feature> are not visible
#    And User login to tuya
#    And User removes PM detector <feature> from the dashboard
#    Then <feature> is not visible at the dashboard
#
#    Examples:
#    |           feature               |
#    |           Stężenie PM10         |
#    |           Stężenie PM1          |
