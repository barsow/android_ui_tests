Feature: Tuya Plug

#  Scenario: Plug includes all needed functions
#    Given Preconfigured account with Tuya Plug
#    When Entering the plug tab
#    Then All plug functions are visible
#
#  Scenario: Plug settings tab and name change work correctly
#    Given Preconfigured account with Tuya Plug
#    When Entering the plug settings tab
#    And Changes the plug name
#    Then New plug name is visible
#
#  Scenario Outline: Add and remove plug features from the dashboard
#    Given Preconfigured account with Tuya Plug
#    When User adds plug <feature> to the dashboard
#    And User removes plug <feature> from the dashboard
#    Then <feature> is not visible at the dashboard
#
#    Examples:
#    |             feature                     |
#    |             Zasilanie                   |
#    |          Zużycie tygodniowe             |
#    |          Zużycie miesięczne             |
#    |          Zużycie roczne                 |
##    |          Natężenie prądu                | #Bug - not visible ont the list
#    |          Pobór energii                  |
##    |          Napięcie                       | #Bug - not visible ont the list
#    |          Nadchodzące zdarzenie          |
#
#  Scenario: Add plug to the room
#    Given Preconfigured account with Tuya Plug
#    When User adds plug to the room via room settings
#    Then Plug is visible in the room settings
#
#  Scenario: Remove plug from the room
#    Given Preconfigured account with Tuya Plug
#    When User removes plug from the room
#    Then Plug is not visible in the room settings
#
#  Scenario Outline: Logout from tuya
#    Given Preconfigured account with Tuya Plug
#    When User adds plug <feature> to the dashboard
#    And User logouts from tuya
#    And Plug and <feature> are not visible
#    And User login to tuya
#    And User removes plug <feature> from the dashboard
#    Then <feature> is not visible at the dashboard
#
#    Examples:
#    |           feature               |
#    |           Zasilanie             |
#    |           Zużycie tygodniowe    |
