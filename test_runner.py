import os
import yagmail
import datetime

APPARTME_ANDROID_PATH = './'
ANDROID_UI_TESTS_PATH = './android_ui_tests'


class TestRunner:

    def __init__(self):
        self.report_filename = ''

    def build_and_download_app(self):
        try:
            os.system('./gradlew jacocoReport')
            os.system('./gradlew tasks')
            os.system('./gradlew clean installStageAppartmeDebug --stacktrace')
        except:
            print("Error when building and downloading the app")

    def execute_tests(self):
        os.chdir(ANDROID_UI_TESTS_PATH)
        os.system('pwd')
        os.system('behave -f allure_behave.formatter:AllureFormatter -o results/ test_scenarios/ --junit')

    def prepare_report(self):
        try:
            os.system('allure generate results/ -o allure/reports --clean')
            self.report_filename = 'results-{}.zip'.format(datetime.datetime.now().isoformat(timespec='minutes'))
            os.system('zip -r {} reports/ results/ '.format(self.report_filename))
        except:
            print("Error when preparing the test report")

    def upload_results(self):
        user = 'bs.bartek.sowa@gmail.com'
        app_password = 'wbnfnpukrviwsyao'
        to = 'bartosz.sowa@slabs.pl'
        subject = 'Testy automatyczne'
        content = ['To podsumowanie testów Appartme wysłane przez automatycznego Testera. Pozdrawiam :-)',
                   '', self.report_filename]
        with yagmail.SMTP(user, app_password) as yag:
            yag.send(to, subject, content)

    def cleanup(self):
        try:
            os.system('rm results/*.json')
        except:
            print("Error when removing the test report")


btr = TestRunner()
btr.build_and_download_app()
btr.execute_tests()
btr.prepare_report()
btr.upload_results()
btr.cleanup()

