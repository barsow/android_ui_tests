from screen_models.scrn_base import ScrnBase
from lib.config import APP_VARIANT


class NewSpaceScrn(ScrnBase):

    def __init__(self, name, driver):
        super().__init__(name, driver)
        self.verifiable_elements = [self.name_field(), self.create_button(), self.title_field(), self.space_image()]

    def name_field(self):
        return self.driver.device(resourceId=APP_VARIANT + ":id/nameInput")\
            .child(resourceId=APP_VARIANT + ":id/input")

    def create_button(self):
        return self.driver.device(resourceId=APP_VARIANT + ":id/button")

    def title_field(self):
        title_txt = self.xml_parser.get_name_value('createSpace__title')
        return self.driver.device(text=title_txt)

    def name_field_hint(self):
        hint_txt = self.xml_parser.get_name_value('createSpace__nameField_Hint')
        return self.driver.device(text=hint_txt)

    def space_image(self):
        return self.driver.device(resourceId=APP_VARIANT + ":id/pickerImage")

    def create_space(self, name):
        self.name_field().set_text(name)
        self.create_button().click()
        self.driver.implicitly_wait()
