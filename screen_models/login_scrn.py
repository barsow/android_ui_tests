from screen_models.scrn_base import ScrnBase
from lib.config import APP_VARIANT


class LoginScrn(ScrnBase):

    def __init__(self, name, driver):
        super().__init__(name, driver)
        self.verifiable_elements = [self.email_field(), self.password_field(), self.login_button()]

    def email_field(self):
        email_txt = self.xml_parser.get_name_value('inputText__emailField_Hint')
        return self.driver.device(text=email_txt)

    def password_field(self):
        password_txt = self.xml_parser.get_name_value('inputText__passwordField_Hint')
        return self.driver.device(text=password_txt)

    def login_button(self):
        return self.driver.device(resourceId=APP_VARIANT + ":id/loginButton")

    def login_user(self, email, password):
        self.driver.implicitly_wait()
        self.email_field().set_text(email)
        self.password_field().set_text(password)
        self.driver.press_system_button("back")
        self.login_button().click()
        self.driver.implicitly_wait()
