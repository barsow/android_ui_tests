from screen_models.scrn_base import ScrnBase
from lib.config import APP_VARIANT


class RoomsScrn(ScrnBase):

    def __init__(self, name, driver):
        super().__init__(name, driver)
        self.verifiable_elements = [self.new_room_button(), self.rooms_tab()]

    def name_field(self):
        return self.driver.device(resourceId=APP_VARIANT + ":id/nameInput")\
            .child(resourceId=APP_VARIANT + ":id/input")

    def create_button(self):
        return self.driver.device(resourceId=APP_VARIANT + ":id/button")

    def toolbar_icon(self):
        return self.driver.session(resourceId=APP_VARIANT + ':id/right_toolbar_icon')

    def change_name_button(self):
        return self.driver.session(text='Zmień nazwę')

    def save_button(self):
        return self.driver.session(text='Zapisz')

    def remove_button(self):
        return self.driver.session(text='Usuń Pokój')

    def rooms_tab(self):
        rooms_tab_txt = self.xml_parser.get_name_value('zonesTab__sectionTitle')
        return self.driver.device(text=rooms_tab_txt)

    def new_room_button(self):
        create_button_txt = self.xml_parser.get_name_value('zonesTab__addZoneButton')
        return self.driver.device(text=create_button_txt)

    def input_field(self):
        return self.driver.session(resourceId=APP_VARIANT + ":id/nameInput").child(
            resourceId=APP_VARIANT + ":id/input")
