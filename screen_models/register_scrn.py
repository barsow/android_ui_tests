from screen_models.scrn_base import ScrnBase
from lib.config import APP_VARIANT


class RegisterScrn(ScrnBase):

    def __init__(self, name, driver):
        super().__init__(name, driver)
        self.verifiable_elements = [self.email_field(), self.password_field(), self.confirm_password_field(),
                                    self.terms_checkbox(), self.register_button()]

    def email_field(self):
        email_txt = self.xml_parser.get_name_value('inputText__emailField_Hint')
        return self.driver.device(text=email_txt)

    def password_field(self):
        password_txt = self.xml_parser.get_name_value('inputText__passwordField_Hint')
        return self.driver.device(text=password_txt)

    def confirm_password_field(self):
        confirm_pass_txt = self.xml_parser.get_name_value('registration__passwordFieldRepeat_Hint')
        return self.driver.device(text=confirm_pass_txt)

    def register_button(self):
        return self.driver.device(resourceId=APP_VARIANT + ":id/registerButton")

    def terms_checkbox(self):
        return self.driver.device(resourceId=APP_VARIANT + ":id/termsCheckboxTick")

    def validation_info(self):
        validation_info_txt = self.xml_parser.get_name_value('registration__validationInfo')
        return self.driver.device(text=validation_info_txt)

    def subtitle_field(self):
        subtitle_txt = self.xml_parser.get_name_value('registration__subtitle')
        return self.driver.device(text=subtitle_txt)

    def register_user(self, email, password, confirm_password):
        self.email_field().set_text(email)
        self.password_field().set_text(password)
        self.confirm_password_field().set_text(confirm_password)
        self.driver.press_system_button("back")
        self.register_button().click()
        self.terms_checkbox().click()
        self.driver.implicitly_wait()
