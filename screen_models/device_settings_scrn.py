from screen_models.scrn_base import ScrnBase
from lib.config import APP_VARIANT


class DevicesSettingsScrn(ScrnBase):

    def __init__(self, name, driver, device_name='', device_room='', device_space=''):
        super().__init__(name, driver)
        self.verifiable_elements_strings = ["Wersja", "Dom ID", "Produkt ID",
                                    "Sprawdź aktualizacje", "Usuń urządzenie", "Zmień nazwę", device_name]
        self.verifiable_elements = []

    def change_name_button(self):
        return self.driver.session(text="Zmień nazwę")

    def new_name_input(self):
        return self.driver.device(resourceId=APP_VARIANT + ":id/input")

    def new_name_confirm_button(self):
        return self.driver.session(text='Zatwierdź')

    def check_updates_button(self):
        return self.driver.session(text='Sprawdź aktualizacje')

    def change_device_name(self, new_name):
        self.change_name_button().click()
        self.new_name_input().set_text(new_name)
        self.new_name_confirm_button().click()
