from screen_models.scrn_base import ScrnBase
from lib.config import APP_VARIANT


class SpacesScrn(ScrnBase):

    def __init__(self, name, driver):
        super().__init__(name, driver)
        self.verifiable_elements = [self.new_space_button(), self.title_field(), self.back_toolbar_icon()]

    def new_space_button(self):
        return self.driver.device(resourceId=APP_VARIANT + ":id/button")

    def title_field(self):
        title_txt = self.xml_parser.get_name_value('availableSpaces__title')
        return self.driver.device(text=title_txt)

    def back_toolbar_icon(self):
        return self.driver.device(resourceId=APP_VARIANT + ":id/back_toolbar_icon")

    def account_tab(self):
        return self.driver.session(resourceId=APP_VARIANT + ":id/accountFragment")

    def tuya_login_arrow(self):
        return self.driver.session(resourceId=APP_VARIANT + ":id/rightImage")[1]

    def click_new_space_button(self):
        self.new_space_button().click()
        self.driver.implicitly_wait()
