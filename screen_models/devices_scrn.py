from screen_models.scrn_base import ScrnBase
from lib.config import APP_VARIANT


class DevicesScrn(ScrnBase):

    def __init__(self, name, driver):
        super().__init__(name, driver)
        self.verifiable_elements = [self.add_device_button(), self.devices_section_title()]

    def add_device_button(self):
        return self.driver.device(resourceId=APP_VARIANT + ":id/button")

    def devices_tab(self):
        return self.driver.session(resourceId=APP_VARIANT + ":id/devicesFragment")

    def devices_section_title(self):
        title_txt = self.xml_parser.get_name_value('devicesTab__sectionTitle')
        return self.driver.device(text=title_txt)
