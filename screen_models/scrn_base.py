import datetime
from time import sleep
from lib.logger import get_logger
from lib.config import DEFAULT_TIMEOUT_S
from lib.xml_parser import XmlParser
logger = get_logger(__name__)


class ScrnBase:

    def __init__(self, name, driver):
        self.name = name
        logger.debug("Add device the element...")
        self.driver = driver
        self.xml_parser = XmlParser()
        self.screenshot_no = 0

    def empty_element(self, element):
        logger.debug("Clearing the element...")
        element.clear_text()

    def get_element_text(self, element):
        text = element.get_text()
        logger.debug("Element text found: {}".format(text))
        return text

    def check_page_elements(self):
        logger.debug("Checking page elements")
        for element in self.verifiable_elements:
            assert element.exists(timeout=DEFAULT_TIMEOUT_S) is True

    def search_element_in_list(self, element):
        max_iterations = 20
        sleep(2)
        for i in range(max_iterations):
            if element.exists():
                return True
            self.driver.device.swipe(0.5, 0.7, 0.5, 0.5)
            sleep(2)
        return False


    @staticmethod
    def save_image(image_name="", delay_sec=0):
        def save_image(method):
            def wrapper(*args):
                args[0].screenshot_no += 1
                method(*args)
                if delay_sec != 0:
                    sleep(delay_sec)
                image = args[0].driver.device.screenshot()
                if not image_name:
                    image.save(str(args[0].screenshot_no) + "_" + args[0].name +
                               str(datetime.datetime.now())[:-7].replace(" ", "_") + ".png")
                else:
                    image.save(image_name)
            return wrapper
        return save_image
