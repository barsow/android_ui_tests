from screen_models.scrn_base import ScrnBase
from lib.config import APP_VARIANT, DEFAULT_TIMEOUT_S


class DashboardScrn(ScrnBase):

    def __init__(self, name, driver):
        super().__init__(name, driver)
        self.verifiable_elements = [self.add_button(), self.dashboard_settings_icon(), self.no_dashboards_message()]
        self.tiles = []

    def add_button(self):
        add_new = self.xml_parser.get_name_value('dashboardsTab__addDashboardButton')
        return self.driver.device(text=add_new)

    def add_function_button(self):
        add_function_txt = self.xml_parser.get_name_value('dashboardsTab__addFunctionButton')
        self.search_element_in_list(self.driver.session(text=add_function_txt))
        return self.driver.session(text=add_function_txt)

    def dashboard_settings_icon(self):
        return self.driver.device(resourceId=APP_VARIANT + ':id/rightImageView')

    def no_dashboards_message(self):
        no_dashboard_txt = self.xml_parser.get_name_value('dashboardsTab__dashboardsEmptyState')
        return self.driver.device(text=no_dashboard_txt)

    def add_dialog_add_button(self):
        button_text = self.xml_parser.get_name_value('addDashboard__confirmButton')
        return self.driver.device(text=button_text)

    def add_dialog_cancel_button(self):
        button_text = self.xml_parser.get_name_value('editDashboards__deleteAbortButton')
        return self.driver.device(text=button_text)

    def add_dialog_hint(self):
        hint_text = self.xml_parser.get_name_value('addDashboard__subtitle')
        return self.driver.device(text=hint_text)

    def dashboard_input_field(self):
        return self.driver.device(resourceId=APP_VARIANT + ":id/input")

    def dashboard_settings_delete_icon(self):
        return self.driver.session(resourceId=APP_VARIANT + ':id/leftImageView')

    def dashboard_settings_edit_icon(self):
        return self.driver.session(resourceId=APP_VARIANT + ':id/rightImageView')

    def dashboard_settings_delete_dialog_confirm(self):
        delete_text = self.xml_parser.get_name_value('editDashboards__deleteConfirmButton')
        return self.driver.session(text=delete_text)

    def edit_dashboard(self, new_name):
        self.dashboard_settings_edit_icon().click()
        self.dashboard_input_field().set_text(new_name)
        self.driver.session(text='Zatwierdź').click()
        self.driver.implicitly_wait()

    def delete_dashboard(self, name):
        self.dashboard_settings_delete_icon().click()
        self.dashboard_input_field().set_text(name)
        self.driver.implicitly_wait()
        self.dashboard_settings_delete_dialog_confirm().click()

    def add_dashboard(self, name):
        self.add_button().click()
        self.dashboard_input_field().set_text(name)
        self.add_dialog_add_button().click()
        self.driver.implicitly_wait()

    def verify_edit_dashboard_dialog(self):
        assert (self.driver.session(text='Zatwierdź').exists(timeout=DEFAULT_TIMEOUT_S))
        assert (self.driver.session(text='Anuluj').exists(timeout=DEFAULT_TIMEOUT_S))

    def verify_dashboard_name_trimmed(self, too_long_name):
        assert(self.driver.session(text=too_long_name[:30]).exists(timeout=DEFAULT_TIMEOUT_S))
        assert (self.dashboard_settings_delete_icon().exists(timeout=DEFAULT_TIMEOUT_S))
        assert (self.dashboard_settings_edit_icon().exists(timeout=DEFAULT_TIMEOUT_S))
