from lib.logger import get_logger, log_request, log_response
import requests

logger = get_logger(__name__)


def send_request(method, url, data=None, headers=None, json=None, params=None, verify=True):
    log_request(method=method.upper(), url=url, data=data, headers=headers, json=json, params=params, verify=verify)

    try:
        function = getattr(requests, method)
        response = function(url=url, data=data, headers=headers, json=json, params=params, verify=verify)
    except ConnectionError as e:
        logger.error('Request failed: %s', e)
    else:
        log_response(response)

        return response
    return None


def send_delete(url, data=None, headers=None, json=None, params=None, verify=True):
    return send_request('delete', url=url, data=data, headers=headers, json=json, params=params, verify=verify)


def send_get(url, data=None, headers=None, json=None, params=None, verify=True):
    return send_request('get', url=url, data=data, headers=headers, json=json, params=params, verify=verify)


def send_head(url, data=None, headers=None, json=None, params=None, verify=True):
    return send_request('head', url=url, data=data, headers=headers, json=json, params=params, verify=verify)


def send_options(url, data=None, headers=None, json=None, params=None, verify=True):
    return send_request('options', url=url, data=data, headers=headers, json=json, params=params, verify=verify)


def send_patch(url, data=None, headers=None, json=None, params=None, verify=True):
    return send_request('patch', url=url, data=data, headers=headers, json=json, params=params, verify=verify)


def send_post(url, data=None, headers=None, json=None, params=None, verify=True):
    return send_request('post', url=url, data=data, headers=headers, json=json, params=params, verify=verify)


def send_put(url, data=None, headers=None, json=None, params=None, verify=True):
    return send_request('put', url=url, data=data, headers=headers, json=json, params=params, verify=verify)
