import string
import random
import hashlib
import uuid


def generate_random_name(only_digits=False, str_length=10):
    letters = string.digits if only_digits else string.ascii_lowercase + string.ascii_uppercase
    return ''.join(random.choice(letters) for i in range(str_length))


def get_string_hash(password):
    return hashlib.sha256(password.encode()).hexdigest()


def get_password_hash(password):
    return hashlib.sha256(password.encode()).hexdigest()


def get_default_user_data():
    password = 'K4' + str(uuid.uuid4())[:6]
    user_data = {"email": "anduitester" + str(uuid.uuid4())[:8] + '@slabs.pl',
                        "password_hash": get_password_hash(password),
                        'active': True}
    return user_data
