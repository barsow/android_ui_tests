from lib.helpers import generate_random_name


class NewZone:

    def get_obj(self):
        data = {"name": generate_random_name(),
         "type": "normal"}
        return data
