
class MetadataExample:

    def get_obj(self):
        return [{
            "name": "user_spaces",
            "value": "1.2 asda asda",
            "type": "string"
        }, {
            "name": "tuya_cred312321entials",
            "value": "1.qwewqeq2 asda asda",
            "type": "string"
        }, {
            "name": "slabs_connect_credentials",
            "value": "1.2 asda asda",
            "type": "string"
        }, {
            "name": "last_password_reset_utc",
            "value": "1.2 asda asda",
            "type": "string"
        }, {
            "name": "last_login_utc",
            "value": "1.AAA2 asda asda",
            "type": "string"
        }]
