import uuid
from lib.helpers import get_string_hash


class NewUser:

    def get_obj(self):
        password = 'K4' + str(uuid.uuid4())[:6]
        email = "anduitester" + str(uuid.uuid4())[:8] + '@slabs.pl'
        password_hash = get_string_hash(password)
        user_data = {"email": email,
                     "password_hash": password_hash,
                     'active': True}
        return user_data, password
