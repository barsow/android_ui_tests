from lib.helpers import generate_random_name


class NewDashboard:

    def get_obj(self):
        return {"name": generate_random_name()}
