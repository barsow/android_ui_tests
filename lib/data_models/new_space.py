import uuid


class NewSpace:

    def get_obj(self):
        name = 'K4' + str(uuid.uuid4())[:20]
        data = {"name": name}
        return data
