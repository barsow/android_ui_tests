from lib.config import HOST
from lib.data_models.new_user import NewUser
from lib.data_models.new_space import NewSpace
from lib.data_models.new_zone import NewZone
from lib.data_models.new_dashboard import NewDashboard
from lib.data_models.metadata_example import MetadataExample


class DataModels:
    def __init__(self):
        self.new_user = NewUser()
        self.new_space = NewSpace()
        self.new_zone = NewZone()
        self.new_dashboard = NewDashboard()
        self.metadata_example = MetadataExample()


class Api:
    def __init__(self):
        self.host = HOST
        self.models = DataModels()

    def auth_v2(self):
        return self.host + 'auth/api/v2/auth'

    def visual_v1_spaces(self):
        return self.host + 'visual/api/v1/spaces'

    def visual_v1_spaces_zones(self, space_uuid):
        return self.host + 'visual/api/v1/spaces/{}/zones'.format(space_uuid)

    def visual_v1_spaces_dashboards(self, space_uuid):
        return self.host + 'visual/api/v1/spaces/{}/dashboards'.format(space_uuid)

    def user_spaces(self, user_id):
        return self.host + 'api/v1/users/{}/spaces'.format(user_id)

    def user_zones(self, user_id, space_id):
        return self.host + \
               'api/v1/users/{}/spaces/{}/zones'.format(user_id, space_id)

    def user_dashboards(self, user_id, space_id):
        return self.host + 'api/v1/users/{}/spaces/{}/dashboards'.format(user_id, space_id)


class ApiFascade(Api):

    def inventory_users(self):
        return self.host + 'api/v1/users'

    def auth_v2(self):
        return self.host + 'api/v1/auth'

    def user_devices(self, user_id):
        return self.host + 'api/v1/users/{}/devices'.format(user_id)

    def user_data(self, user_id):
        return self.host + 'api/v1/users/{}/data'.format(user_id)

    def zones_devices(self, user_id, space_id, zone_id):
        return self.host + 'api/v1/users/{}/spaces/{}/zones/{}/devices'.format(user_id, space_id, zone_id)

    def user_shares(self, user_id):
        return self.host + 'api/v1/users/{}/shares'.format(user_id)

    def user_device_shares(self, user_id, device_id):
        return self.host + 'api/v1/users/{}/devices/{}/shares'.format(user_id, device_id)

    def v1_spaces_dashboards(self, user_id, space_uuid):
        return self.host + 'api/v1/users/{}/spaces/{}/dashboards'.format(user_id, space_uuid)
