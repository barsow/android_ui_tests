from time import sleep
from lib.config import APP_VARIANT, ELEMENT_WAIT_S, SYS_CONFIRM_DIALOG_TEXT
from lib.logger import get_logger
logger = get_logger(__name__)


class DeviceDriver:

    def __init__(self, device):
        self.device = device

    def set_session(self, session):
        self.session = session

    def start_app(self, app_name=APP_VARIANT, clear_app=True):
        if clear_app:
            self.device.app_clear(app_name)
        self.device.app_start(app_name)

    def get_running_aps_list(self):
        return self.device.app_list_running()

    def get_device_info(self):
        return self.device.info

    def press_system_button(self, button_name):
        # possible keys: home, back, left, right, up, down, center, menu
        # search, enter, delete, recent, volume_up, volume_down, volume_mute
        # camera, power
        logger.debug("Pressing {}".format(button_name))
        self.device.press(button_name)

    def implicitly_wait(self, delay=ELEMENT_WAIT_S):
        logger.debug("Implicitly wait for {}s".format(delay))
        sleep(delay)

    def turn_on_wifi(self):
        self.device.shell('svc wifi enable')

    def turn_off_wifi(self):
        self.device.shell('svc wifi disable')

    def turn_on_bluetooth(self):
        self.device.shell('am start -a android.bluetooth.adapter.action.REQUEST_ENABLE')
        self.device(text=SYS_CONFIRM_DIALOG_TEXT).click()

    def turn_off_bluetooth(self):
        self.device.shell('am start -a android.bluetooth.adapter.action.REQUEST_DISABLE')
        self.device(text=SYS_CONFIRM_DIALOG_TEXT).click()
