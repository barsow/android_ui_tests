import os

HOST = 'https://core-stage.appartme.cloud/'
APP_VARIANT = 'pl.appartme.stage.debug'

USER_EMAIL = 'bartek.rudowski@gmail.com\n'
USER_PASSWORD = 'Haslomaslo1\n'
USER_ID = '63b7a1ff-349d-4d4f-afa3-fefaf30da7ce'
TUYA_EMAIL = 'bartek.rudowski@gmail.com\n'
TUYA_PASSWORD = 'Haslomaslo1\n'

SPACE_NAME = "TestSpace"
SPACE_ID = '869ca488-f2a9-43eb-bf58-2cf791669431'
DASHBOARD_NAME = "TestDashboard"

PLUG_ROOM = "PLUG ROOM"
PLUG_NAME = "Smart Socket"

TUYA_CONTACTRON_NAME = "okno"
TUYA_CONTACTRON_ROOM = "Contactron"
TUYA_CONTACTRON_ROOM_ID = '07c0735a-6480-44a2-8f37-6f7c4dbc607f'
TUYA_CONTACTRON_ID = '1f94ce4c-cfe8-42c1-9526-0fb769a3c5f0'
TUYA_GATEWAY_NAME = "gateway"

TUYA_TRV_NAME = "kaloryfer kuchnia"
TUYA_TRV_ROOM = "TRV"
TUYA_TRV_ROOM_ID = 'f9ad7abc-3902-49ae-8365-d2e3d6d31488'
TUYA_TRV_ID = '0b6958da-befb-46fc-bb0a-3b7f1f2a9dbb'

TUYA_SENSOR_NAME = "Czujnik w kuchni"
TUYA_SENSOR_ROOM = "SENSOR ROOM"
TUYA_SENSOR_ROOM_ID = 'bf3686ff-c4b7-4b1a-afb6-ba1605168e13'
TUYA_SENSOR_ID = '5b6f7fdc-dabb-462b-97c4-eac2dd78f4d2'

TUYA_POWERSTRIP_NAME = "Listwa"
TUYA_POWERSTRIP_ROOM = "Listwa room"
TUYA_POWERSTRIP_ROOM_ID = "4d86327b-75ff-465b-9988-eea4e291d8b0"
TUYA_POWERSTRIP_ID = "f8305031-dc7f-4292-81a1-be7da6e851f7"

PM_DETECTOR_NAME = "PM Detector"
PM_DETECTOR_ROOM = "PM DETECTOR ROOM"

DEFAULT_TIMEOUT_S = 10
ELEMENT_WAIT_S = 4
SYS_CONFIRM_DIALOG_TEXT = 'ZEZWALAJ'
