from lib.helpers import get_string_hash
from lib.rest_client import *
from lib.api import ApiFascade
from lib.config import *


class RestUser:
    def __init__(self):
        self.api = ApiFascade()
        self.user_token = None
        self.admin_token = None
        self.email = None
        self.password = None
        self.password_hash = None
        self.user_id = None

    def get_default_headers(self, authorize=True, external_token=None):
        headers = {
            'Content-Type': 'application/json'
        }
        if authorize:
            if external_token:
                headers['Authorization'] = 'Bearer {}'.format(external_token)
            else:
                if not self.user_token:
                    self.user_token = self.login_and_get_token({'email': self.email, 'password_hash': self.password_hash})
                headers['Authorization'] = 'Bearer {}'.format(self.user_token)
        return headers

    def register_account(self, user_data=None, expected_status_code=200):
        if not user_data:
            user_data, password = self.api.models.new_user.get_obj()
            self.password = password
        self.email = user_data['email']
        self.password_hash = user_data['password_hash']
        response = send_post(url=self.api.inventory_users(), json=user_data, headers=self.get_default_headers(False))
        assert response.status_code == expected_status_code
        return response

    def login_and_get_token(self, user):
        headers = {'Content-Type': 'application/json'}
        data = {
            'email': user['email'],
            'password_hash': user['password_hash']
        }
        response = send_post(url=self.api.auth_v2(), json=data, headers=headers)
        token = response.json()

        assert response.status_code == 200
        assert 'response' in token
        assert 'access_token' in token['response']
        return token['response']['access_token']

    def logout(self):
        headers = self.get_default_headers()
        response = send_delete(url=self.api.auth_v2(), headers=headers)
        assert response.status_code == 200

    def generate_admin_token(self):
        admin = {
            'email': 'admin@admin.admin',
            'password_hash': get_string_hash('inventory')
        }
        self.admin_token = self.login_and_get_token(admin)

    def activate_account(self, user_id):
        self.generate_admin_token()
        headers = self.get_default_headers(True, self.admin_token)
        data = {
            'active': True
        }
        return send_patch(url=self.api.inventory_users() + '/' + str(user_id), headers=headers, json=data)

    def create_space(self, user_id):
        headers = self.get_default_headers()
        space_data = self.api.models.new_space.get_obj()
        return send_post(url=self.api.user_spaces(user_id), headers=headers, json=space_data)

    def create_zone(self, user_id, space_id):
        headers = self.get_default_headers()
        zone_data = self.api.models.new_zone.get_obj()
        return send_post(url=self.api.user_zones(user_id, space_id), headers=headers, json=zone_data)

    def create_dashboard(self, user_id, space_id, dashboard_data=None):
        headers = self.get_default_headers()
        if not dashboard_data:
            dashboard_data = self.api.models.new_dashboard.get_obj()
        return send_post(url=self.api.user_dashboards(user_id, space_id), headers=headers, json=dashboard_data)

    def update_dashboard(self, user_id, space_id, dashboard_id, dashboard_data):
        headers = self.get_default_headers()
        return send_patch(url=self.api.user_dashboards(user_id, space_id), headers=headers, json=dashboard_data)

    def delete_dashboard(self, user_id, space_id, dashboard_id):
        headers = self.get_default_headers()
        return send_delete(url=self.api.v1_spaces_dashboards(user_id, space_id) + '/{}'.format(dashboard_id),
                           headers=headers)

    def get_dashboard_id_by_name(self, user_id, space_id, name):
        response = self.get_user_dashboards(user_id, space_id)
        for elem in response.json()['response']:
            if elem['name'] == name:
                return elem['id']
        return False

    def get_user_zones(self, user_id, space_id):
        headers = self.get_default_headers()
        return send_get(url=self.api.user_zones(user_id, space_id), headers=headers)

    def get_user_dashboards(self, user_id, space_uuid):
        url = self.api.v1_spaces_dashboards(user_id, space_uuid)
        headers = self.get_default_headers()
        return send_get(url, headers=headers)

    def get_user_data(self, user_id):
        headers = self.get_default_headers()
        return send_get(url=self.api.inventory_users() + '/' + str(user_id), headers=headers)

    def delete_user_account(self, user_id):
        headers = self.get_default_headers(True, self.admin_token)
        return send_delete(url=self.api.inventory_users() + '/' + str(user_id), headers=headers)

    def delete_user_account(self, user_id):
        headers = self.get_default_headers()
        return send_delete(url=self.api.inventory_users() + '/' + str(user_id), headers=headers)

    def get_users(self):
        headers = self.get_default_headers(True, self.admin_token)
        return send_get(url=self.api.inventory_users(), headers=headers)

    def get_user_devices(self, user_id):
        headers = self.get_default_headers(True, self.admin_token)
        return send_get(url=self.api.user_devices(user_id), headers=headers)

    def find_device_in_users(self, device_id):
        reponse_users = self.get_users().json()['response']
        user_ids = []
        for user in reponse_users:
            response = self.get_user_devices(user['id'])
            try:
                devices = response.json()['response']
            except:
                continue
            for device in devices:
                if device['id'] == device_id:
                    print(user['email'])
                    user_ids.append(user['id'])
        return user_ids

    def delete_user_device(self, user_id, device_id):
        headers = self.get_default_headers(True, self.admin_token)
        return send_delete(url=self.api.user_devices(user_id) + '/' + device_id, headers=headers)

    def send_user_data(self, user_id):
        example_data = self.api.models.metadata_example.get_obj()
        headers = self.get_default_headers()
        return send_post(url=self.api.user_data(user_id), headers=headers, json=example_data)

    def password_reset(self, user_email):
        data = {'email': user_email}
        return send_post(url=self.api.inventory_users() + '/account/password', json=data)

    def activation(self, user_email):
        data = {'email': user_email}
        return send_post(url=self.api.inventory_users() + '/account/activation', json=data)

    def get_device_report(self, user_id, device_id):
        headers = self.get_default_headers()
        return send_get(url=self.api.user_devices(user_id=user_id) +
                        '/{}'.format(device_id) + '/summary/year?year=1969', headers=headers)

    def add_user_device(self, user_id, device_data):
        headers = self.get_default_headers()
        return send_post(url=self.api.user_devices_api_v2(user_id=user_id), headers=headers, json=device_data)

    def delete_device_from_room(self, user_id, space_id, zone_id, device_id):
        headers = self.get_default_headers(True, self.admin_token)
        return send_delete(url=self.api.zones_devices(user_id, space_id, zone_id) + '/{}'.format(device_id),
                           headers=headers)

    def get_user_shares(self, user_id):
        headers = self.get_default_headers()
        return send_get(url=self.api.user_shares(user_id=user_id), headers=headers)

    def get_user_device_shares(self, user_id, device_id):
        headers = self.get_default_headers()
        return send_get(url=self.api.user_device_shares(user_id, device_id), headers=headers)

    def set_device_permission(self, user_id, device_id, user_email):
        headers = self.get_default_headers()
        return send_post(url=self.api.user_device_shares(user_id, device_id),
                         headers=headers, json={'email': user_email})

    def accept_sharing(self, user_id, share_id):
        headers = self.get_default_headers()
        return send_patch(url=self.api.user_shares(user_id=user_id) + '/{}'.format(share_id),
                         headers=headers, json={"status": "accepted"})

    def upload_csr(self, user_id, share_id):
        headers = self.get_default_headers()
        return send_post(url=self.api.user_shares(user_id=user_id) + '/{}/requests'.format(share_id),
                         headers=headers, json={"csr": "JBf5ig1zgl/vnF7LbGJvAI7T8xOGJQ1VtUKurmgSlcgBZxN7bNJBbZHrzEMYgra4OlQDn76iwTWANtALAF5j/5nrrvvxwW43xYSyzIBi68OovnrfJiTSTKTeiTGVYD2UL8U1V92FJ++8KbxK5A0nSiM7IL78Aq47XSAQAGJWmgFZRZ3Wo3T85FC+gNF+LG2q8XgT7vI22XDqEtMCocQsCmWuqPf/ld3pXBekzaeM7dsQu+jQrMXeeE8H3zbkT/qI/LtA3NXU4+DE7OgHvbKSuWsBTrSedLf5Ys9xx7JJciJbD4etUQZ8yMhHOmW4ZoRgPx5uftdltXtwjEmGKMoneghET6Py/8piBddJwvaMD6TbgTgUTsq0iuB62qhTaXkhkYj19KHASKFRagfJYqGZay6S/gyw3BAbkE9v9Mcd7FBz0fKQjYC5avZsCW0kj6x15AkxZtGNE6o8GFIvYQGoKVLcU+2Xr2MzR+q1i9CESA/Z8VxYt/hIsI5/l66+4XHBBlcfAIB0TBud1iPKCZbgr8c2vzLzwzffKJ4nnUabakk8B0lmPQy7p9ntQvNTf7oq9jBchZYtH82SkMJrPDbXJUk8eNTJSplMlkLKCq4/tnwIwCIgEE8JSoI1vv9O0TsUW+NxlQEDLvehIDRGFZOoAkxHa6XV2NOW88fQ91YPd/B+kbrJmp6qWGsFTLaOEl2enK+nuLuy1ILDVi4gmb2fk62EfVrIWqcHT8kyBlRhGyPhxGfANSfezUEu794Dc86avP1WUu1lj/y58SJpKcmyu5HqKVhSCkm2N4GPjOuMAW273mh0j2YOVowC90dNZ1FMdTCKSzC9QxOfYwYbekLdd/619ai+PqthcY6r/xAav9jTEzT9PZbwcFNHaKWEUexmfRLbVpalZvvk0muXBvuIRsQ1ZJVGQwAOBldpmNNK3SCLpxP5EG7b8gZ2M4+4nYhOwaotcaUbige/Yye4lBxgDfLRy1Ti/VVUTyYnFZZx/nRgDSql1+P7LLptT2mX6ZC5vZja0K+CbkL836LoyZbFzf/JhUagJ7VifZFtuMx5DC48cmNMwqFMfOXR10zcdVJ/Gv+ZQgQujaGVKN7ExRMYPVFNm+HWIxFPnFhjjgo6V17mX8q7bJAGI5oV4wEeacfRabNDg9fNGpNyrrkZ4igTeL1Nk9+keBwEPU2zAMXvP21HOQorZR1bK5PnmxKebrazd0qGFo1At1ByVYrM3V8sBfyeA/KseJx65XlyXgJaPgGYGP7diWbhpb8h/gmC5Owh93Sc4H1N6a/oBAO3smWMTRN5p5sAXrlZBMLyQSJsY7u38B43QXfoYUkoCs99jPUUw0jp3VeQG7+RWkhoQc1M8w=="})


    def get_csr(self, user_id, share_id):
        headers = self.get_default_headers()
        return send_get(url=self.api.user_shares(user_id=user_id) + '/{}/requests'.format(share_id),
                         headers=headers)

    def send_csr(self, share_id):
        headers = self.get_default_headers()
        return send_post(url='https://core-stage.appartme.cloud/api/v1/shares/{}/certificates'.format(share_id),
                         headers=headers, json={"certificate": "certificateinbase64"})

    def get_certificates(self, user_id, share_id):
        headers = self.get_default_headers()
        return send_get(url=self.api.user_shares(user_id=user_id) + '/{}/certificates'.format(share_id),
                         headers=headers)

    def send_sign_csr(self, share_id):
        headers = self.get_default_headers()
        headers = {
            'Content-Type': 'application/json'
        }
        return send_post(url='https://192.168.0.213:8000/api/' + 'v1/connect/sign_csr', headers=headers,
        json={"csr": "JBf5ig1zgl/vnF7LbGJvAI7T8xOGJQ1VtUKurmgSlcgBZxN7bNJBbZHrzEMYgra4OlQDn76iwTWANtALAF5j/5nrrvvxwW43xYSyzIBi68OovnrfJiTSTKTeiTGVYD2UL8U1V92FJ++8KbxK5A0nSiM7IL78Aq47XSAQAGJWmgFZRZ3Wo3T85FC+gNF+LG2q8XgT7vI22XDqEtMCocQsCmWuqPf/ld3pXBekzaeM7dsQu+jQrMXeeE8H3zbkT/qI/LtA3NXU4+DE7OgHvbKSuWsBTrSedLf5Ys9xx7JJciJbD4etUQZ8yMhHOmW4ZoRgPx5uftdltXtwjEmGKMoneghET6Py/8piBddJwvaMD6TbgTgUTsq0iuB62qhTaXkhkYj19KHASKFRagfJYqGZay6S/gyw3BAbkE9v9Mcd7FBz0fKQjYC5avZsCW0kj6x15AkxZtGNE6o8GFIvYQGoKVLcU+2Xr2MzR+q1i9CESA/Z8VxYt/hIsI5/l66+4XHBBlcfAIB0TBud1iPKCZbgr8c2vzLzwzffKJ4nnUabakk8B0lmPQy7p9ntQvNTf7oq9jBchZYtH82SkMJrPDbXJUk8eNTJSplMlkLKCq4/tnwIwCIgEE8JSoI1vv9O0TsUW+NxlQEDLvehIDRGFZOoAkxHa6XV2NOW88fQ91YPd/B+kbrJmp6qWGsFTLaOEl2enK+nuLuy1ILDVi4gmb2fk62EfVrIWqcHT8kyBlRhGyPhxGfANSfezUEu794Dc86avP1WUu1lj/y58SJpKcmyu5HqKVhSCkm2N4GPjOuMAW273mh0j2YOVowC90dNZ1FMdTCKSzC9QxOfYwYbekLdd/619ai+PqthcY6r/xAav9jTEzT9PZbwcFNHaKWEUexmfRLbVpalZvvk0muXBvuIRsQ1ZJVGQwAOBldpmNNK3SCLpxP5EG7b8gZ2M4+4nYhOwaotcaUbige/Yye4lBxgDfLRy1Ti/VVUTyYnFZZx/nRgDSql1+P7LLptT2mX6ZC5vZja0K+CbkL836LoyZbFzf/JhUagJ7VifZFtuMx5DC48cmNMwqFMfOXR10zcdVJ/Gv+ZQgQujaGVKN7ExRMYPVFNm+HWIxFPnFhjjgo6V17mX8q7bJAGI5oV4wEeacfRabNDg9fNGpNyrrkZ4igTeL1Nk9+keBwEPU2zAMXvP21HOQorZR1bK5PnmxKebrazd0qGFo1At1ByVYrM3V8sBfyeA/KseJx65XlyXgJaPgGYGP7diWbhpb8h/gmC5Owh93Sc4H1N6a/oBAO3smWMTRN5p5sAXrlZBMLyQSJsY7u38B43QXfoYUkoCs99jPUUw0jp3VeQG7+RWkhoQc1M8w=="})


# ru =RestUser()
# ru.user_token = ru.login_and_get_token({'email': 'bartek.rudowski@gmail.com', 'password_hash': get_string_hash('Haslomaslo1')})
# resp = ru.set_device_permission('63b7a1ff-349d-4d4f-afa3-fefaf30da7ce', '6befd23c-a3c4-4c7b-9560-e819b2c5a1cb', 'bs.bartek.sowa@gmail.com')
# ru.user_token = ru.login_and_get_token({'email': 'bs.bartek.sowa@gmail.com', 'password_hash': get_string_hash('Kupa1234')})
# ru.accept_sharing('5d9aa53c-fbb0-4625-8517-c46afd5f360d', resp.json()['response']['id'])
# ru.upload_csr('5d9aa53c-fbb0-4625-8517-c46afd5f360d', resp.json()['response']['id'])
# ru.accept_sharing('63b7a1ff-349d-4d4f-afa3-fefaf30da7ce', '9e3b66a1-5315-4409-a8eb-02ae56ae7454')
# ru.user_token = ru.login_and_get_token({'email': 'bs.bartek.sowa@gmail.com', 'password_hash': get_string_hash('Kupa1234')})
# ru.get_user_device_shares('5d9aa53c-fbb0-4625-8517-c46afd5f360d', '6befd23c-a3c4-4c7b-9560-e819b2c5a1cb')
# ru.get_certificates('5d9aa53c-fbb0-4625-8517-c46afd5f360d', '22cc90a0-1bde-4beb-9424-45198b5cf96e')
# ru.upload_csr('5d9aa53c-fbb0-4625-8517-c46afd5f360d', 'fe823950-2e86-48ce-b0e0-a2f7b7dfca0a')
# ru.user_token = ru.login_and_get_token( {'email': 'bartek.rudowski@gmail.com', 'password_hash': get_string_hash('Haslomaslo1')})
# ru.get_csr('63b7a1ff-349d-4d4f-afa3-fefaf30da7ce', 'fe823950-2e86-48ce-b0e0-a2f7b7dfca0a')
# ru.send_sign_csr(resp.json()['response']['id'])
# ru.upload_csr('63b7a1ff-349d-4d4f-afa3-fefaf30da7ce', '2e1c7aea-a09d-4971-b776-21796d92899d')
# ru.get_user_shares('63b7a1ff-349d-4d4f-afa3-fefaf30da7ce')
