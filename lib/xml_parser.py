import xml.etree.ElementTree as ET


class XmlParser:

    def __init__(self, filename='lib/strings.xml', string_attribute='name'):
        self.filename = filename
        tree = ET.parse(filename)
        self.root = tree.getroot()
        self.string_attribute = string_attribute
        self.dict = {}

    def create_dict(self):
        found = self.root.findall("./")
        for elem in found:
            self.dict[elem.attrib[self.string_attribute]] = elem.text

    def get_name_value(self, name):
        found = self.root.findall("./string[@name='{}']".format(name))
        return found[0].text


# use example:
# par = XmlParser()
# par.create_dict()
# print(par.get_name_value('common__cancel'))
