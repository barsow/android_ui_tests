import logging
import requests


loggers = {}
# logger levels: info, debug, warning, error, critical


def get_logger(name=None):
    global loggers

    if name not in loggers:
        formatter = logging.Formatter('[%(asctime)s] [%(filename)s] [%(lineno)d] [%(levelname)s] %(message)s')

        ch = logging.StreamHandler()
        ch.setFormatter(formatter)

        logger = logging.getLogger(name)
        logger.setLevel(logging.DEBUG)
        logger.addHandler(ch)

        loggers[name] = logger

    return loggers[name]


def log_call(function):
    def wrapper(*args, **kwargs):
        args_str = ', '.join([str(arg) for arg in args])
        kwargs_str = ', '.join([str(kwarg) for kwarg in kwargs.values()])

        logger = get_logger(__name__)
        logger.debug('{}({}, {})'.format(function.__name__, args_str, kwargs_str))
        return_value = function(*args, **kwargs)
        logger.debug('{}({}, {}): {}'.format(function.__name__, args_str, kwargs_str, return_value))
        return return_value
    return wrapper


def log_request(method: str, url: str, data=None, headers=None, json=None, params=None, verify=True):
    logger = get_logger(__name__)
    logger.debug('Sending %s request to %s', method, url)
    if data:
        logger.debug('Sending %s request with data %s', method, data)
    if headers:
        logger.debug('Sending %s request with headers %s', method, headers)
    if json:
        logger.debug('Sending %s request with json %s', method, json)
    if params:
        logger.debug('Sending %s request with params %s', method, params)
    if verify:
        logger.debug('Sending %s request with verify %s', method, verify)


def log_response(response: requests.Response):
    logger = get_logger(__name__)
    logger.debug('Received response %s with code %s', response.text, response.status_code)
